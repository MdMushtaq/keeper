<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table = 'cart';

    public function getTotalAttribute()
    {
        return $this->product->price * $this->productquantity;
    }
    
    public function getTotalFormattedAttribute()
    {
        return number_format($this->total, 2);
    }


    public function product()
    {
        return $this->belongsTo(products::class, 'productid');
    }
}
