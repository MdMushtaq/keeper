<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\banners;

class ContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    function index()
    {
        $banners = new banners;
        $data["banners"] = $banners::all();
        return view('content.banner', $data);
    }

    function create()
    {
        return view('content.banneradd');
    }

    function createpost(Request $request)
    {

        $banner = new banners;
        $banner->name = $request->name;
        $banner->arabicname = $request->arabic_name;
        $banner->save();

        return redirect('admin/banners');

    }

    function banneredit($bannerid)
    {
        $banner = new banners;
        $data["banner"] = $banner::where('id',$bannerid)->first();
        return view('content.banneredit', $data);
    }

    function bannereditpost(Request $request)
    {
        $banner = new banners;

        $updateColumns = array(
            'name'=>$request->name,
            'arabicname'=>$request->arabic_name
        );

        $banner::where('id', $request->bannerid)->update($updateColumns);

        return redirect('admin/banners');
    }
}
