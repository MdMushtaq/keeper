<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Categories;
use App\subcategories;
use App\Cart;
use App\products;
use App\Wishlist;
use App\User;
// use App\Session;
use App\Orders;
use App\Orderitem;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except([
            // 'cartadd'
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orderitem = Orders::with('orderitems')->get();

        $wishlist = Wishlist::with('products')->where('user_id',auth()->id())->get();
        return view('front-end.customer_dashboard',compact('orderitem','wishlist'));
    }

    public function cartadd(Request $request,$id) {
     
        // if (!auth()->check()) {
        //     $request->session()->put('url.intended', route('products.list', $id));
        //     $request->session()->put('product', [
        //         'product_id' => $id,
        //         'quantity' => $request->qty
        //     ]);   

        //     return redirect()->route('login');
        // }
        
        $cart = new Cart;
        $cart->user_id = Auth::user()->id;
        $cart->productid = $id;
        $cart->productcolorid = 'blue';
        $cart->productquantity = $request->qty;
        $cart->status = 0;
        $cart->save();

        return redirect()->back()->with('success','Successfully Added to Your Cart');
        
    }

    public function checkoutcart()
    {         
        $cartItems = Cart::with('product')->where(['user_id' => auth()->id(), 'status' => 0])->get();
        return view('front-end.checkout',compact('cartItems'));
    }

    public function wishlist($id)
    {
       $wishlist = new Wishlist;
       $wishlist->product_id = $id;
       $wishlist->user_id = auth()->id();
       $wishlist->save();

       return back()->with('success','Successfully Added to Your Wishlist');

    }
    public function checkoutForm()
    {
        $cartItems = Cart::with('product')->where(['user_id' => auth()->id(), 'status' => 0])->get();
        
        $customerinfo = User::where('id', auth()->id())->first(); 
        
        return view('front-end.checkout-form',compact('cartItems','customerinfo'));
        # code...
    }

    public function Checkoutpost(Request $request)
    {
        $order = new Orders;
        $order->user_id = auth()->id();
        $order->shipping_name = $request->shipping_name;
        $order->shipping_phone = $request->shipping_phone;
        $order->shipping_address = $request->shipping_address;
        $order->status = 'Pending';
        $order->approved_status = '';
        $order->vat = $request->vat;
        $order->total = $request->total;
        $order->shipping_price = '';
        $order->subtotal = $request->subtotal;
        $order->save();

        $cartItems = Cart::with('product')->where('user_id', auth()->id())->get();

        foreach($cartItems as $orderitem){
            $product = $orderitem->product;

            $items = new Orderitem;
            $items->order_id = $order->id;
            $items->product_id = $product->id;
            $items->product_name = $product->prod_name;
            $items->product_unit_price = $product->price;
            $items->product_quantity = $orderitem->productquantity;
            $items->product_price = $product->price * $orderitem->productquantity;
            $items->save();

            // dd($items);
        }        
        $cartDelete = Cart::where('user_id',auth()->id()); 
        $cartDelete->delete();

        return view('front-end.transaction');

      
    }

    public function CustomerDashboard()
    {
       $orderitem = Orders::with('orderitems')->get();

       $wishlist = Wishlist::with('products')->where('user_id',auth()->id())->get();
        return view('front-end.customer_dashboard',compact('orderitem','wishlist'));
    }
}
