<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\Productattributes;
use App\Popularproducts;
use App\Orderitem;
use App\Orders;
use DB;

class ReportController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {

        $best_sell = products::with('order_item','productQuantity')->get();
        // $productQuantity = products::with('')->get();
        return view("report.products_report",compact('best_sell'));
    }
    public function SalesReport()
    {
        $orders = Orders::with('orderitems')->get();
        return view("report.sales_report",compact('orders'));
    }

    // public function reportpost(Request $request)
    // {
    //     foreach($request->popularproducts as $popularproduct)
    //     {
    //         $popularProudct = new Popularproducts;
    //         $popularProudct->productid = $popularproduct;
    //         $popularProudct->status = 1;
    //         $popularProudct->save();
    //         $checkingProductRow = Popularproducts::where("productid", $popularproduct)->first();

    //         return redirect()->back();
    //     }
    // }

}
