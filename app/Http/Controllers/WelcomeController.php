<?php

namespace App\Http\Controllers;

use App\brand_promotions;
use App\category_promotions;
use Illuminate\Http\Request;

use App\Categories;
use App\subcategories;
use App\products;
use App\Productattributes;
use App\Cart;
use Auth;
use App\product_promotions;
use App\subcategory_promotions;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendProductUrlEmail;
use App\Subscriptions;



class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getproducts = products::where(['status' => 1 , 'addhome' => 'on'])->orderBy('created_at','desc')->limit(32)->get();
        return view('front-end.welcome',compact('getproducts'));

        // return view('front-end.welcome');
    }

    function filterprice(Request $request)
    {
        // dd($request->all());
        
        $getproducts  = products::whereBetween('price',[$request->min_price, $request->max_price])->where(['status' => 1,])->get();
        return view('front-end.welcome',compact('getproducts'));
       
    }

    public function newproduct()
    {
        $getproducts = products::where(['status' => 1 , 'addNewproduct' => 'on'])->orderBy('created_at','desc')->limit(32)->get();
        return view('front-end.welcome',compact('getproducts'));
    }

    public function popularproduct()
    {
        $getproducts = products::where(['status' => 1 , 'addPopular' => 'on'])->orderBy('created_at','desc')->limit(32)->get();
        return view('front-end.welcome',compact('getproducts'));
    }

    public function topProduct()
    {
        $getproducts = products::where(['status' => 1 , 'addTopSell' => 'on'])->orderBy('created_at','desc')->limit(32)->get();
        return view('front-end.welcome',compact('getproducts'));
    }

    function Categorylist($name)
    {
        $categoriesImage = Categories::where(['cat_name' => $name])->first();
        // $subCategories = subcategories::with('products')->where('parent_cat_id' , $id)->get();
        $getproducts = products::where(['categories_id' => $categoriesImage->id , 'status' => 1])->orderBy('created_at','desc')->get();
        return view('front-end.welcome',compact('getproducts','categoriesImage'));

    }

    function SubCategorylist($id)
    {
        $getproducts = products::where(['sub_category_id' => $id , 'status' => 1])->orderBy('created_at','desc')->get();
        return view('front-end.welcome',compact('getproducts'));

    }

    

    function productslist(Request $request, $id)
    {
        // if ($request->session()->has('product') && auth()->check()) {
        //     $controller = new HomeController();
        //     $product = $request->session()->pull('product');
        //     $request->request->add(['qty' => $product['quantity']]);
        //     $controller->cartadd($request, $product['product_id']);
        // }
       
        $list = products::find($id);
        $productattributes = Productattributes::where("productid",$id)->get();

        return view('front-end.productslist',compact('list','productattributes'));
    }


    public function offers()
    {
        $offers = product_promotions::orderBy('created_at','desc')->get();
        $categoriesoffers = category_promotions::orderBy('created_at','desc')->get();
        $subcategoriesoffers = subcategory_promotions::orderBy('created_at','desc')->get();
        $brandsoffers = brand_promotions::orderBy('created_at','desc')->get();

        return view('front-end.offers',compact('offers','subcategoriesoffers','brandsoffers','categoriesoffers'));
    }

    public function comparepage(Request $request)
    {
        $productids = $request->productids;

        $data["products"] = array();
        $data["products1"] = array();

        foreach($productids as $productid)
        {
            $products = products::where('id', $productid)->first();
            array_push($data["products"], $products);
            array_push($data["products1"], $products);

        }

        return view('front-end.comparision', $data);

    }

    public function sendproductemail(Request $request)
    {
        $data = array(
          "producturl"=>$request->producturl
        );
        Mail::to($request->email)->send(new SendProductUrlEmail($data));

        return back()->with('success','Product Url has been sent');
    }

    public function subscribe(Request $request)
    {
        $subscriptions = new Subscriptions;
        $subscriptions->email = $request->email;
        $subscriptions->save();

        return back()->with("successfulsubsciption",'Email subscribed for newsletter');
    }
  
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
