<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wishlist;
use App\User;
use App\products;

class WishlistController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $data["wishlist"] = Wishlist::all();
        return view("wishlist", $data);
    }
}
