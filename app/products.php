<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    //

    protected $table = 'products';

    public function order_item()
    {
       return $this->hasMany(OrderItem::class, 'product_id');
    }
   
    public function productQuantity()
    {
       return $this->hasMany(productattributes::class, 'productid');
    }
   
}   
   