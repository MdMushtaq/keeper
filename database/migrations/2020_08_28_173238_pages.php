<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title')->nullable();
            $table->string('arabic_title')->nullable();
            $table->string('menu_title')->nullable();
            $table->string('menu_arabic_title')->nullable();
            $table->string('content')->nullable();
            $table->string('arabic_content')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('metadata')->nullable();
            $table->string('slug')->nullable();
            $table->string('sequence')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
