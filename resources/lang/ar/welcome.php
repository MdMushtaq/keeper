<?php
   return [
      // welcome.blade.php
 
      'View All' => 'مشاهدة الكل',
      'New Products' => 'منتجات جديدة',
      'Popular Products' => 'المنتجات الشعبية',
      'Top Selling Products' => 'المنتجات الأكثر مبيعًا',
      'Filter Product' => 'مرشح المنتج',
      'SAR' => 'ريال سعودي',
      'ADD TO CART' => 'أضف إلى السلة',
      'CART' => 'عربة التسوق',


      // layouts front-app.blade.php
      'FREE AND ESAY RETURNS' => 'عوائد مجانية وسهلة',
      'BEST DEALS' => 'افضل العروض',
      'TOP BRANDS' => 'ارقى الماركات',
      'SIGN IN' => 'تسجيل الدخول',
      'SIGN UP' => 'سجل',
      'Select Language' => 'اختار اللغة',

      // Header menu Bar
      'CATEEGORIES' => 'التصنيفات',
      'Electronics' => 'إلكترونيات',
      'PRODUCTS' => 'منتجات',
      'SPECIAL OFFER' => 'عرض خاص',
      'Offer' => 'عرض',
      'ABOUT US' => 'معلومات عنا',
      'HELP' => 'مساعدة',
      'CONTACT US' => 'اتصل بنا',
      'Contact Us' => 'اتصل بنا',
      'Complaint' => 'شكوى',
      'SHOP MENU' => 'قائمة التسوق',
      
   ];
?>