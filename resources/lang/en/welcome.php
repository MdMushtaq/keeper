<?php
   return [
      // welcome.blade.php
    
      'View All' => 'View All',
      'New Products' => 'New Products',
      'Popular Products' => 'Popular Products',
      'Top Selling Products' => 'Top Selling Products',
      'Filter Product' => 'Filter Product',
      'SAR' => 'SAR',
      'ADD TO CART' => 'ADD TO CART',
      'CART' => 'CART',


      // layouts front-app.blade.php
      'FREE AND ESAY RETURNS' => 'FREE AND ESAY RETURNS',
      'BEST DEALS' => 'BEST DEALS',
      'TOP BRANDS' => 'TOP BRANDS',
      'SIGN IN' => 'SIGN IN',
      'SIGN UP' => 'SIGN UP',
      'Select Language' => 'Select Language',

      // Header menu Bar
      'CATEEGORIES' => 'CATEEGORIES',
      'Electronics' => 'Electronics',
      'PRODUCTS' => 'PRODUCTS',
      'SPECIAL OFFER' => 'SPECIAL OFFER',
      'Offer' => 'Offer',
      'ABOUT US' => 'ABOUT US',
      'HELP' => 'HELP',
      'CONTACT US' => 'CONTACT US',
      'Contact Us' => 'Contact Us',
      'Complaint' => 'Complaint',
      'SHOP MENU' => 'SHOP MENU',
      
   ];
?>