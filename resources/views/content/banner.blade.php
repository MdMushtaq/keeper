@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Banner Collections</h1>
</div>
<a class="btn btn-primary pull-right" href="{{ url('admin/banners/add') }}"><i class="icon-plus"></i> Add New Banner Collection</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Arabic Name</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($banners as $banner)
    <tr>
        <td>{{ $banner->name }}</td>
        <td>{{ $banner->arabicname }}</td>
        <td class="text-right">
            <div class="btn-group">
                <a class="btn btn-default" href="#">
                    <i class="icon-image"></i>
                </a>
                <a class="btn btn-default" href="{{ url('admin/banners/edit/') }}<?= '/'.$banner->id; ?>">
                    <i class="icon-pencil"></i>
                </a>
            </div>
        </td>
    </tr>
    @endforeach


    </tbody>
</table>
@endsection