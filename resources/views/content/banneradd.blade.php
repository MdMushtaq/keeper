@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Add New Banner Collections</h1>
</div>
<form action="{{ url('admin/banners/addpost') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="title">Name </label>
                <input type="text" name="name" value="" class="form-control">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="title">Arabic Name </label>
                <input type="text" name="arabic_name" value="" class="form-control arabic-input" lang="ar" dir="rtl">
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <input class="btn btn-primary" type="submit" value="Save">
        </div>
        <div class="col-md-10"></div>
    </div>
</form>
@endsection