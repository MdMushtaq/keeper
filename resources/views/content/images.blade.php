@extends('layouts.admin-app')
@section('content')

<div class="page-header">
	<h1>Images</h1>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Sort</th>
            <th>Name</th>
            <th>Arabic Name</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="banners_sortable" class="ui-sortable">
        <tr id="banners-5">
            <td class="handle">
                <a class="btn btn-primary">
                    <span class="icon-sort"></span>
                </a>
            </td>
            <td>header-ad-1</td>
            <td>header-ad-1</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#">
                        <i class="icon-pencil"></i>
                    </a>
                </div>
            </td>
        </tr>
        <tr id="banners-6">
            <td class="handle">
                <a class="btn btn-primary">
                    <span class="icon-sort"></span>
                </a>
            </td>
            <td>header-ad-2</td>
            <td>header-ad-2</td>
            <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#">
                        <i class="icon-pencil"></i>
                    </a>
                </div>
            </td>
        </tr>
            <tr id="banners-7">
            <td class="handle">
                <a class="btn btn-primary">
                    <span class="icon-sort"></span>
                </a>
            </td>
            <td>header-ad-3</td>
            <td>header-ad-3</td>
                <td class="text-right">
                <div class="btn-group">
                    <a class="btn btn-default" href="#">
                        <i class="icon-pencil"></i>
                    </a>
                </div>
            </td>
        </tr>
    </tbody>
</table>

@endsection