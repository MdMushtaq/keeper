@extends('layouts.admin-app')
@section('content')

@if(count($errors))
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.
    <br/>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>

</div>

@endif
@if(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
@endif



<div class="container">

    <!--          <h1>Good</h1>-->
    <!--          <h1>Yes</h1>-->


    <div class="page-header">
        <h1>Customer Form</h1>
    </div>
    <script type="text/javascript">
        function areyousure()
        {
            return confirm('Are you sure you want to delete this customer?');
        }
    </script>
    <form action="{{ url('/admin/customers/form') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
<!--        <div class="row">-->
<!--            <div class="col-md-3">-->
<!--                <div class="form-group">-->
<!--                    <label>Company</label>-->
<!--                    <input type="text" name="company" value="" class="form-control">-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="name" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastname" value="" class="form-control">
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-3">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Gender</label>
                    <select name="gender" class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Confirm</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Country</label>
                    <select name="country" class="form-control" required>
                        <?php
                        foreach($countries as $country)
                        {
                            ?>
                            <option value="<?= $country->id; ?>" {{ $country->code == 'SA' ? 'selected' : ''}}><?= $country->name; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>City</label>
                    <input type="text" class="form-control" name="city" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <input type="radio" name="status" value="1" checked>
                <label>Active</label>
                <input type="radio" name="status" value="2">
                <label>Archive</label>
                <input type="radio" name="status" value="0">
                <label>Suspend</label>
            </div>
        </div>

        <div class="row hidden">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Group</label>
                    <select name="group_id" class="form-control">
                        <option value="1">Retail</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-10">
                <input class="btn btn-primary" type="submit" value="Save">
            </div>
            <div class="col-md-2">
            </div>
        </div>

    </form>
    <hr>
    <footer></footer>
</div>

@endsection

