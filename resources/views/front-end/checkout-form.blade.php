@extends('layouts.front-app')
@section('content')
<div class="container">
	@php
	$cartCount = 0; 
	$cartCount = \App\Cart::where(['user_id' => auth()->id(), 'status' => 0])->count();
	@endphp

	@if($cartCount != 0)
    <div class="py-5 text-center">
        <h2>Checkout</h2>
        <p class="lead">Need Some Informtion"</p>
    </div>
    <div class="stepwizard col-md-offset-3">
    	<div class="stepwizard-row setup-panel">
      		<div class="stepwizard-step">
        		<a href="#step-1" type="button" class="btn in-active-step btn-circle">1</a>
        		<p>Account</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-2" type="button" class="btn btn-default btn-circle btn-primary" disabled="disabled">2</a>
        		<p>Address</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-3" type="button" class="btn btn-default btn-circle in-active-step" disabled="disabled">3</a>
        		<p>Payment</p>
      		</div>
    	</div>
  	</div>
    <div class="row">
        <div class="col-md-4 order-md-2 mt-4 mb-4 border-gray p-0">
            <div class="payment">
            	<h4 class="mt-3 ml-4">Cart - item(s)</h4>
            </div>	
			
			
			@foreach ($cartItems as $item)
				@php $product = $item->product; @endphp
				<div class="col-md-12 mt-4 mb-4 border-bottom-img">
					<div class="row product-container">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="product-img">
								@foreach (json_decode($product->image) as $key => $products)

									@if($key == 0)
										<img height="100" width="100" src="{{asset('public/images/products/'.$products)}}" class="img-thumbnail rounded" >
									@endif
								@endforeach
							</div>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
						<p class="product-text">{{$product->prod_name}}</p>
							<p>{{$item->productquantity}} x  {{$item->total_formatted}}</p>
						</div>
					</div>	
				</div>
			@endforeach

    	
    		<div class="payment">
            	<h4 class="mt-3 ml-4">Total</h4>
            </div>

    		<div class="col-md-12 mt-4 mb-4 border-bottom-img">
    			<div class="row product-container">
	        		<div class="col-md-12 col-sm-12 col-xs-12">
	            		<table class="table">
							@php 
							$total = $cartItems->sum('total');
							$vatotal = App\Vat::where('vatselect',1)->first();
							$vat = $total * $vatotal->vatpercentage / 100; 
							@endphp
                			<tbody>
                				<tr>
                					<td> Subtotal:</td>
                					<td class="text-right">SR {{number_format($total, 2)}}</td>
								</tr>
								<tr>
									<td>
										<strong>VAt 15%:</strong>
									</td>
									<td class="text-right">
										<strong>SR {{number_format($vat, 2)}}</strong>
									</td>
								</tr>
  								<tr>
  									<td>
  										<strong>Total:</strong>
  									</td>
  									<td class="text-right">
  										<strong>SR {{ number_format($total + $vat, 2) }}</strong>
  									</td>
								</tr>
                			</tbody>
            			</table>
	        		</div>
	        	</div>	
    		</div>
        </div>

        <div class="col-md-8 order-md-1 mt-4 mb-4">
        	<div class="payment">
            	<h4 class="mt-3 ml-4">Payment & Shipping Address</h4>
            </div>	
                <div class="row mt-3">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">First name</label>
					<input type="text" class="form-control" id="firstName" value="{{$customerinfo->name}}" placeholder="First name" readonly>
                        
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Last name</label>
                        <input type="text" class="form-control" id="lastName" value="{{$customerinfo->lastname}}" placeholder="Last name" readonly>
                        
                    </div>
                </div>
                <div class="mb-3">
                    <label for="email">Email <span class="text-muted">(Optional)</span></label>
                    <input type="email" class="form-control" value="{{$customerinfo->email}}" id="email" placeholder="Email" readonly>
                </div>
                <div class="mb-3">
                    <label for="mobile-number">Mobile Number</label>
                    <input type="tel" class="form-control" value="{{$customerinfo->phone}}" id="mobile-number" placeholder="Mobile number" readonly>
                    
                </div>	
				<div class="mb-3">
                    <label for="country">Country</label>
                    <input type="text" class="form-control" id="country" value="{{$customerinfo->country}}" readonly>
                    
				</div>
				<div class="mb-3">
                    <label for="city">City</label>
                    <input type="text" class="form-control" id="city" value="{{$customerinfo->city}}" readonly>
                    
                </div>

                <div class="mb-3">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" value="{{$customerinfo->address}}" readonly>
                    
                </div>
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="ship-to-another">
                    <label class="custom-control-label" for="ship-to-another">Shipping to another address</label>
				</div>

				<form action="{{route('checkout-form.post')}}" method="POST" >
					@csrf

					<input type="hidden" name="total" value="{{number_format($total, 2)}}">
					<input type="hidden" name="vat" value="{{number_format($vat, 2)}}">
					<input type="hidden" name="subtotal" value="{{ number_format($total + $vat, 2) }}">

					<div id="changeShipInputs">
						<div class="mb-3">
							<label for="person-name">Name</label>
							<input type="text" class="form-control" name="shipping_name" id="person-name" placeholder="Name" >
						
						</div>

						<div class="mb-3">
							<label for="person-mob-no">Mobile Number</label>
							<input type="tel" class="form-control" id="person-mob-no" name="shipping_phone" placeholder="Mobile Number" >
						
						</div>

						<div class="mb-3">
							<label for="person-mob-no">Address</label>
							<input type="text" class="form-control" id="another-ship-address" name="shipping_address" placeholder="Another ship address" > 
						</div> 
					</div>    
					<button class="btn btn-primary btn-lg btn-block" type="submit">Save Address</button>
				</form>
        </div>
    </div>
    @else
        
		<div class="text-center">
			<p>There are no items in this cart</p>
			<a href="{{ url('/') }}" type="button" class=" btn btn-success">Continue Shoping</a>
		</div>
	@endif
</div>
@endsection