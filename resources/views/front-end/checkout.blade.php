@extends('layouts.front-app')
@section('content')

{{-- @php 
$cartCount = 0;
if (auth()->check()) {
    $cartCount = \App\Cart::where(['user_id' => auth()->id(), 'status' => 0])->get();
    $productclist = 
}
@endphp --}}

<div class="container py-5 text-center">
	<h2>Cart</h2>
	<p class="lead">Need Some Informtion"</p>
</div>
<div class="px-4 px-lg-0">
	<div class="pb-5">
        @php 
        $cartCount = 0; 
        $cartCount = \App\Cart::where(['user_id' => auth()->id(), 'status' => 0])->count();
        @endphp
        
        @if($cartCount != 0)
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 pl-5 pr-5 bg-white rounded">
					<div class="table-responsive">
						<table class="table cart-table" cellspacing="0">
							<thead>
								<tr>
									<th class="product-remove">&nbsp;</th>
									<th class="product-thumbnail">Image</th>
									<th class="product-name">Product</th>
									<th class="product-price">Price</th>
									<th class="product-quantity">Quantity</th>
									<th class="product-subtotal">Total</th>
								</tr>
							</thead>
							<tbody>
                               
                                @foreach ($cartItems as $item)
										@php $product = $item->product; @endphp
									
										
											<tr>
												<td class="product-remove">
													<a href="#" class="remove">
														<i class="fa fa-times"></i>
													</a>						
												</td>

												<td class="product-thumbnail">
													@foreach (json_decode($product->image) as $key => $products)

														@if($key == 0)

															<a href="#">
															<img width="100" width="100" src="{{asset('public/images/products/'.$products)}}" class="img-fluid rounded shadow-sm" alt="" >
															</a>
														@endif
													@endforeach
												
												</td>

												<td class="product-name">
												<a href="#">{{$product->prod_name}}</a>
												</td>

												<td class="product-price">
													<span class="amount">
														<bdi>
															<span class="currencySymbol">SR</span> {{$product->price}}
														</bdi>
													</span>
												</td>

												<td class="product-quantity" style="text-align: -webkit-center;">
													<div class="number-input md-number-input">
														<button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
														<input class="quantity" min="0" name="quantity" value="{{$item->productquantity}}" type="number">
														<button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
														
														
													</div>
													
												</td>

												<td class="product-subtotal">
													<span class="price-amount amount">
														<bdi>
															<span class="currencySymbol">SR</span> {{$item->total_formatted}}
														</bdi>
													</span>						
												</td>
											</tr>
								@endforeach
								
							</tbody>
						</table>
					</div>
				</div>
			</div>

			{{--<div class="row bg-white">
				<div class="col-lg-6">
                        
                    <div class="actions">
                        <div class="coupon">
                            <div class="input-group mb-4 border rounded p-2">
                            <input type="text" placeholder="Apply coupon" aria-describedby="button-addon3" class="form-control border-0">
                                <div class="input-group-append border-0">
                                    <button id="button-addon3" type="button" class="btn btn-dark px-4 rounded btn-primary">
                                        <i class="fa fa-gift mr-2"></i>Apply coupon</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-6">
                    <div class="actions text-center">
                        <div class="input-group mb-4 rounded-pill">
                            <div class="input-group-append border-0">
                                <button id="button-addon3" type="button" class="btn btn-dark px-4 rounded-pill btn-primary">
                                    Update</button>
                            </div>
                        </div>
                        
                    </div>
                </div> 

				
			</div>--}}
p
			@php 
				$total = $cartItems->sum('total');
				$vatotal = App\Vat::where('vatselect',1)->first();
                $vat = $total * $vatotal->vatpercentage / 100; 
			@endphp
			<div class="row bg-white">
                <div class="col-lg-6">
					<a href="{{ url('/') }}" type="button" style="    margin-top: 300px;" class="btn btn-dark  btn-primary">Continue Shoping</a>
				</div>
                <div class="col-lg-6 rounded-pill">
					<div class="px-4 py-3 text-uppercase font-weight-bold" style="font-size:30px;">Cart totals
					</div>
				  	<div class="p-4" style="border: 1px solid;">
					    <ul class="list-unstyled mb-4">
					    	<li class="d-flex justify-content-between py-3 border-bottom">
								<strong class="text-muted">Order Subtotal </strong>
								
							<strong>SR {{number_format($total, 2)}}</strong>
					    	</li>
					      	{{-- <li class="d-flex justify-content-between py-3 border-bottom">
					      		<strong class="text-muted">Shipping and handling</strong>
					      		<strong>$10.00</strong></li> --}}
					      	<li class="d-flex justify-content-between py-3 border-bottom">
								<strong class="text-muted">VAT 15%</strong>
					      		<strong>SR {{number_format($vat, 2)}}</strong>
					      	</li>
					      	<li class="d-flex justify-content-between py-3 border-bottom">
								  <strong class="text-muted">Total</strong>
								
							  <h5 class="font-weight-bold"> {{ number_format($total + $vat, 2) }}</h5>
					      	</li>
					    </ul>
				    	<a href="{{route('checkout.form')}}" class="btn btn-dark rounded py-2 btn-block btn-primary">Procceed to checkout</a>
				  </div>
				</div>
            </div>
	
        </div>
        @else
        
        <div class="text-center">
			<p>There are no items in this cart</p>
        	<a href="{{ url('/') }}" type="button" class="btn btn-dark  btn-primary">Continue Shoping</a>
        </div>
        @endif
       
  	</div>
</div>




@endsection