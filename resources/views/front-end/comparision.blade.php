@extends('layouts.front-app')

@section('content')
<style>
    td{
        color: black;
    }
</style>

<section class="pb-5" style="background-image:url(http://localhost/saidaliah/public/frontend/img/background.jpg);background-repeat:repeat;padding-top:50px;padding-bottom: 50px; ">
    <h1 style="color: #39509c; font-family: sans-serif;">Products Comparision</h1>
    <div class="container">
        <table class="table" style="border: 1px solid black">
            <thead>
            <tr>
                <th style="background-color: white;"></th>
                @foreach($products as $productlist)
                <th>
                    @foreach (json_decode($productlist->image) as $key => $products)
                    @if($key == 0)
                    <img class="img-responsive w-100 " height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                    @endif

                    @if($key == 1)
                    <img class="img-responsive w-100 hover" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                    @endif
                    @endforeach
                </th>
                @endforeach
            </tr>

            </thead>
            <tbody>
            <tr>
                <td>Name</td>
                <td>
                    @if(!empty($products1[0]))
                    {{ $products1[0]->prod_name }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    {{ $products1[1]->prod_name }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    {{ $products1[2]->prod_name }}
                    @endif
                </td>
                <td>
                    dsf
                    @if(!empty($products1[3]))
                    {{ $products1[3]->prod_name }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Brand</td>
                <td>
                    @if(!empty($products1[0]))
                    {{ $products1[0]->brands }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    {{ $products1[1]->brands }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    {{ $products1[2]->brands }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[3]))
                    {{ $products1[1]->brands }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Description</td>
                <td>
                    @if(!empty($products1[0]))
                    {{ $products1[0]->description }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    {{ $products1[1]->description }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    {{ $products1[2]->description }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[3]))
                    {{ $products1[3]->description }}
                    @endif
                </td>

            </tr>
            <tr>
                <td>Specification</td>
                <td>
                    @if(!empty($products1[0]))
                    {{ $products1[0]->specification }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    {{ $products1[1]->specification }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    {{ $products1[2]->specification }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[3]))
                    {{ $products1[3]->specification }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Shipping</td>
                <td>
                    @if(!empty($products1[0]))
                    {{ $products1[0]->shipping }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    {{ $products1[1]->shipping }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    {{ $products1[2]->shipping }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[3]))
                    {{ $products1[3]->shipping }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Weight</td>
                <td>
                    @if(!empty($products1[0]))
                    {{ $products1[0]->weight }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    {{ $products1[1]->weight }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    {{ $products1[2]->weight }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[3]))
                    {{ $products1[3]->weight }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Price</td>
                <td>
                    @if(!empty($products1[0]))
                    {{ $products1[0]->price }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    {{ $products1[1]->price }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    {{ $products1[2]->price }}
                    @endif
                </td>
                <td>
                    @if(!empty($products1[3]))
                    {{ $products1[3]->price }}
                    @endif
                </td>
            </tr>
            <tr>
                <td></td>

                <td>
                    @if(!empty($products1[0]))
                    <a href="#" class="card-hover">
                        <button id="btnFA" class="btn btn-primary btn-xs">
                            <i class="fa fa-shopping-cart"></i>
                            {{ trans('welcome.ADD TO CART')}}
                        </button>
                    </a>
                    @endif
                </td>
                <td>
                    @if(!empty($products1[1]))
                    <a href="#" class="card-hover">
                        <button id="btnFA" class="btn btn-primary btn-xs">
                            <i class="fa fa-shopping-cart"></i>
                            {{ trans('welcome.ADD TO CART')}}
                        </button>
                    </a>
                    @endif
                </td>
                <td>
                    @if(!empty($products1[2]))
                    <a href="#" class="card-hover">
                        <button id="btnFA" class="btn btn-primary btn-xs">
                            <i class="fa fa-shopping-cart"></i>
                            {{ trans('welcome.ADD TO CART')}}
                        </button>
                    </a>
                    @endif
                </td>
                <td>
                    @if(!empty($products1[3]))
                    <a href="#" class="card-hover">
                        <button id="btnFA" class="btn btn-primary btn-xs">
                            <i class="fa fa-shopping-cart"></i>
                            {{ trans('welcome.ADD TO CART')}}
                        </button>
                    </a>
                    @endif
                </td>


            </tr>
            </tbody>
        </table>
    </div>
    <div class="masonry">

    </div>



</section>

@endsection