@extends('layouts.front-app')

@section('content')
<section class="pb-5" style="background-image:url(http://localhost/saidaliah/public/frontend/img/background.jpg);background-repeat:repeat;padding-top:50px;padding-bottom: 50px; ">
    <div class="masonry">

    <!-- Product Offers -->
        <h1>Products</h1>
        @foreach($offers as $offer)

        @php
        $getproducts = App\products::where("all_offers",$offer->id)->where("promotion_type", "product")->get();

        date_default_timezone_set("Asia/Karachi");
        $currentTime = date("Y-m-d H:i:s");
        $end_time = $offer->disable_on_utc;

        $time1 = new DateTime($currentTime);
        $time2 = new DateTime($end_time);
        $interval = $time1->diff($time2);

        $day = $interval->format('%dDay ');
        $hour = $interval->format('%hhour ');
        $min = $interval->format('%imin ');
        $sec = $interval->format('%ssecond ');

        $timeremaining = $day.$hour.$min.$sec;
        @endphp

        @if($end_time >= $currentTime)

        @foreach ($getproducts as $productlist)
        <div class="item">
            <a href="{{route('products.list',$productlist->id)}}">
                <div class="product-block grid-v1">
                    <div class="hovereffect">

                        @foreach (json_decode($productlist->image) as $key => $products)
                        @if($key == 0)
                        <img class="img-responsive w-100 " height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif

                        @if($key == 1)
                        <img class="img-responsive w-100 hover" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif
                        @endforeach
                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                <h3 class="name">
                                    @if(\Session::get('locale') == 'ar')
                                    <a href="#">{{$productlist->arabic_name}}</a>
                                    @else
                                    <a href="#">{{$productlist->prod_name}}</a>
                                    @endif
                                </h3>
                                <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ trans('welcome.SAR')}} {{$productlist->price}}</bdi>
                                    </span>
                                </span>
                                <br>
                                    <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ $timeremaining }}</bdi>
                                    </span>
                                </span>

                            </div>
                        </div>
                        <div class="overlay">
                            <div class="social-icons">
                                <a href="#" class="card-hover">
                                    <button id="btnFA" class="btn btn-primary btn-xs">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('welcome.ADD TO CART')}}
                                    </button>
                                </a>
                                <a href="#" data-tip="Add to Wishlist">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                                <a href="" data-tip="Add to Cart">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach

        @endif
        @endforeach
        </div>

    <div class="masonry">
        <!-- Sub category promotion products -->

        <h1>Categories</h1>
        @foreach($categoriesoffers as $offer)
        @php
        $getproducts = App\products::where("all_offers",$offer->id)->where("promotion_type", "category")->get();

        date_default_timezone_set("Asia/Karachi");
        $currentTime = date("Y-m-d H:i:s");
        $end_time = $offer->disable_on_utc;

        $time1 = new DateTime($currentTime);
        $time2 = new DateTime($end_time);
        $interval = $time1->diff($time2);

        $day = $interval->format('%dDay ');
        $hour = $interval->format('%hhour ');
        $min = $interval->format('%imin ');
        $sec = $interval->format('%ssecond ');

        $timeremaining = $day.$hour.$min.$sec;
        @endphp

        @if($end_time >= $currentTime)

        @foreach ($getproducts as $productlist)
        <div class="item">
            <a href="{{route('products.list',$productlist->id)}}">
                <div class="product-block grid-v1">
                    <div class="hovereffect">

                        @foreach (json_decode($productlist->image) as $key => $products)
                        @if($key == 0)
                        <img class="img-responsive w-100 " height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif

                        @if($key == 1)
                        <img class="img-responsive w-100 hover" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif
                        @endforeach
                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                <h3 class="name">
                                    @if(\Session::get('locale') == 'ar')
                                    <a href="#">{{$productlist->arabic_name}}</a>
                                    @else
                                    <a href="#">{{$productlist->prod_name}}</a>
                                    @endif
                                </h3>
                                <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ trans('welcome.SAR')}} {{$productlist->price}}</bdi>
                                    </span>
                                </span>
                                <br>
                                    <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ $timeremaining }}</bdi>
                                    </span>
                                </span>

                            </div>
                        </div>
                        <div class="overlay">
                            <div class="social-icons">
                                <a href="#" class="card-hover">
                                    <button id="btnFA" class="btn btn-primary btn-xs">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('welcome.ADD TO CART')}}
                                    </button>
                                </a>
                                <a href="#" data-tip="Add to Wishlist">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                                <a href="" data-tip="Add to Cart">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach

        @endif
        @endforeach

    </div>

    <div class="masonry">
        <!-- Sub category promotion products -->

        <h1>SubCategories</h1>
        @foreach($subcategoriesoffers as $offer)

        @php
        $getproducts = App\products::where("all_offers",$offer->id)->where("promotion_type", "subcategory")->get();

        date_default_timezone_set("Asia/Karachi");
        $currentTime = date("Y-m-d H:i:s");
        $end_time = $offer->disable_on_utc;

        $time1 = new DateTime($currentTime);
        $time2 = new DateTime($end_time);
        $interval = $time1->diff($time2);

        $day = $interval->format('%dDay ');
        $hour = $interval->format('%hhour ');
        $min = $interval->format('%imin ');
        $sec = $interval->format('%ssecond ');

        $timeremaining = $day.$hour.$min.$sec;
        @endphp

        @if($end_time >= $currentTime)

        @foreach ($getproducts as $productlist)
        <div class="item">
            <a href="{{route('products.list',$productlist->id)}}">
                <div class="product-block grid-v1">
                    <div class="hovereffect">

                        @foreach (json_decode($productlist->image) as $key => $products)
                        @if($key == 0)
                        <img class="img-responsive w-100 " height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif

                        @if($key == 1)
                        <img class="img-responsive w-100 hover" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif
                        @endforeach
                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                <h3 class="name">
                                    @if(\Session::get('locale') == 'ar')
                                    <a href="#">{{$productlist->arabic_name}}</a>
                                    @else
                                    <a href="#">{{$productlist->prod_name}}</a>
                                    @endif
                                </h3>
                                <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ trans('welcome.SAR')}} {{$productlist->price}}</bdi>
                                    </span>
                                </span>
                                <br>
                                    <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ $timeremaining }}</bdi>
                                    </span>
                                </span>

                            </div>
                        </div>
                        <div class="overlay">
                            <div class="social-icons">
                                <a href="#" class="card-hover">
                                    <button id="btnFA" class="btn btn-primary btn-xs">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('welcome.ADD TO CART')}}
                                    </button>
                                </a>
                                <a href="#" data-tip="Add to Wishlist">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                                <a href="" data-tip="Add to Cart">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach

        @endif
        @endforeach

    </div>
    <div class="masonry">
        <!-- Brand promotion products -->
        <h1>Brand</h1>
        @foreach($brandsoffers as $offer)

        @php
        $getproducts = App\products::where("all_offers",$offer->id)->where("promotion_type", "brands")->get();

        date_default_timezone_set("Asia/Karachi");
        $currentTime = date("Y-m-d H:i:s");
        $end_time = $offer->disable_on_utc;

        $time1 = new DateTime($currentTime);
        $time2 = new DateTime($end_time);
        $interval = $time1->diff($time2);

        $day = $interval->format('%dDay ');
        $hour = $interval->format('%hhour ');
        $min = $interval->format('%imin ');
        $sec = $interval->format('%ssecond ');

        $timeremaining = $day.$hour.$min.$sec;
        @endphp

        @if($end_time >= $currentTime)

        @foreach ($getproducts as $productlist)
        <div class="item">
            <a href="{{route('products.list',$productlist->id)}}">
                <div class="product-block grid-v1">
                    <div class="hovereffect">

                        @foreach (json_decode($productlist->image) as $key => $products)
                        @if($key == 0)
                        <img class="img-responsive w-100 " height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif

                        @if($key == 1)
                        <img class="img-responsive w-100 hover" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                        @endif
                        @endforeach
                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                <h3 class="name">
                                    @if(\Session::get('locale') == 'ar')
                                    <a href="#">{{$productlist->arabic_name}}</a>
                                    @else
                                    <a href="#">{{$productlist->prod_name}}</a>
                                    @endif
                                </h3>
                                <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ trans('welcome.SAR')}} {{$productlist->price}}</bdi>
                                    </span>
                                </span>
                                <br>
                                    <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ $timeremaining }}</bdi>
                                    </span>
                                </span>

                            </div>
                        </div>
                        <div class="overlay">
                            <div class="social-icons">
                                <a href="#" class="card-hover">
                                    <button id="btnFA" class="btn btn-primary btn-xs">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('welcome.ADD TO CART')}}
                                    </button>
                                </a>
                                <a href="#" data-tip="Add to Wishlist">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                                <a href="" data-tip="Add to Cart">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach

        @endif
        @endforeach
    </div>
</section>

@endsection