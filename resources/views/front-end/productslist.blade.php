@extends('layouts.front-app')
@section('content')


<section>
	<div class="container p-0">
        @if (\Session::has('success'))
        <div class="alert alert-success">
        <h3>{{ \Session::get('success') }} </h3>
        </div>
        @endif
		<div class="row">
			<div class="col-md-12">
				<nav class="nav d-block">
			        <ol class="breadcrumb no-bg pl-0 pr-0">
			            <li class="breadcrumb-item">
			            	<a href="#">Home</a></li>
			            <li class="breadcrumb-item">
                            @php $cat =App\Categories::where('id',$list->categories_id)->first(); @endphp
			            	<a href="#">{{$cat->cat_name}}</a>
                        </li>
                        @php $subcat =App\subcategories::where('id',$list->sub_category_id)->first(); @endphp
			            <li class="breadcrumb-item active">{{$subcat->sub_cat_name}}</li>
			        </ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="magnific" class="mb-5">
	<div class="container p-0">
		<div class="row">
			<div class="col-md-5">
			    <div class="xzoom-container"> 
                    @foreach (json_decode($list->image) as $key => $products)
                        @if($key == 0)
                        <img class="xzoom img-fluid"  id="xzoom-magnific"  src="{{asset('public/images/products/'.$products)}}" xoriginal="{{asset('public/images/products/'.$products)}}" />
                        @endif
                    
                    @endforeach
                    <div class="xzoom-thumbs"> 
                            @foreach (json_decode($list->image) as $key => $products)
                            <a href="{{asset('public/images/products/'.$products)}}">
                                <img class="xzoom-gallery" width="60" src="{{asset('public/images/products/'.$products)}}" xpreview="{{asset('public/images/products/'.$products)}}">
                            </a> 
                            @endforeach
                       	<a href="#myModal" data-toggle="modal">
                            <img class="xzoom-gallery-video" width="60" src="{{asset('public/frontend/img/products/charger-thumb-6.jpg')}}">
                        </a>
                    </div>
                </div>      
			</div>
		  	<div class="col-md-6 offset-md-1">
		  		<div class="table-responsive">
                    @php 
                        $vat = App\Vat::where('vatselect',1)->first();
                        $vatotal = $list->price * $vat->vatpercentage / 100; 
                        $total = $vatotal + $list->price;
                    @endphp
					<table class="table table-sm table-borderless mb-0">
					  	<tbody>
                            <tr class="brand">
                                <th class="pl-0" style="width: 37%;" scope="row">
                                    <strong style="font-size:30px !important;">{{$list->prod_name}}</strong>
                                </th>
                            </tr>
                            <tr class="pl-0 w-25">
                                <th class="pl-0 w-25" scope="row">
                                    <strong>Price SR</strong>
                                </th>
                                <td>SR : {{$list->price}}</td>
                            </tr>

                          
                            <tr>
                                <th class="pl-0 w-25" scope="row">
                                    <strong>Vat 15%</strong>
                                </th>
                                <td>SR : {{$vatotal}}</td>
                            </tr>

                            <tr>
                                <th class="pl-0 w-25" scope="row">
                                    <strong>Total Price</strong>
                                </th>
                                <td>SR : {{$total}}</td>
                            </tr>
                            <tr class="brand">
						      	<th class="pl-0 w-25" scope="row">
                                    <strong >Specification</strong>
                                     
						      	</th>
                            </tr>
                                <tr>
                                    <th class="pl-0 w-25">
                                        
                                        <strong>{{$list->specification}}</strong>
                                            
                                    </th>
                                
                                </tr>
					  	</tbody>
                    </table>
                    <form action="{{route('cart.added',$list->id)}}" method="POST">
                        @csrf

                        {{-- <input type="text" id="userqty" name="userqty" > --}}
                       
                        
					<div class="row mt-3 mb-3">
						<div class="col-sm-4">
							<div class="input-group">
					          	<span class="input-group-btn minus bg-primary">
					              	-
					          	</span>
	      						<input type="number" class="form-control input-number count" name="qty" id="qty" value="1">
	      						
					          	<span class="input-group-btn plus bg-primary">
					          		+
					          	</span>
	  						</div>
							
						</div> 
					
						<div class="col-sm-8">	
							<ul class="social">
                                <li>
                                    <button id="btnFA" type="submit" class="btn btn-primary btn-xs" >
                                        <i class="fa fa-shopping-cart"></i>
                                        ADD TO CART
                                    </button>
                                
                                </li>
                                <li>
                                    <a href="{{route('wishlist.add',$list->id)}}" data-tip="Add to Wishlist">
                                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    </a>
                                    {{-- @php $wishlist_add_remmmove = App\Wishlist::where('product_id',$list->id)->first();@endphp

                                    @if($wishlist_add_remmmove->product_id == '')

                                        <a href="{{route('wishlist.update',$list->id)}}" data-tip="Add to Wishlist">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                        </a>
                                    
                                    @else
                                      
                                    @endif --}}
                                   
                                   
                                    
                                </li>
                                   
                                <li>
                                    <a href="" data-tip="Add to Cart">
                                        <i class="fa fa-share-alt"></i>
                                    </a>
                                </li>
                            </ul>
						</div> 
					</div> 
                    <h5 class="colors">
                        Colors:
                        
                        @foreach($productattributes as $productattribute)
                        
                         <span onclick="color_picker()" class="color {{$productattribute->colourname}}"></span>
                        
                        @endforeach

                    </h5>
                </form>
                    
          		</div>
		  	</div>
              
		</div>
	</div>	
</section>  

<section class="">
	<div class="container p-0">
		<div class="row">
			<div class="col-md-12">
				<div class="classic-tabs pt-1">
					<ul class="nav tabs-primary" id="advancedTab" role="tablist">
					    <li class="nav-item">
					    	<a class="nav-link active show" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
					    </li>

					    <li class="nav-item">
					    	<a class="nav-link" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Additional Information</a>
					    </li>

					    
					</ul>

					<div class="tab-content" id="advancedTabContent">
				    	<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
				      		
                        <p class="pt-1">{{$list->description }}</p>
				    	</div>

					    <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
					    	
						    <table class="table table-striped table-bordered mt-3">
						        <thead>
							        <tr>
							        	<th scope="row" class="w-150 dark-grey-text h6">Weight</th>
							            <td><em>0.3 kg</em></td>
							       	</tr>
						        </thead>
						        <tbody>
						        	<tr>
						            	<th scope="row" class="w-150 dark-grey-text h6">Dimensions</th>
						            	<td><em>50 × 60 cm</em></td>
						          	</tr>
						        </tbody>
						    </table>
					    </div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</section>

{{-- @php $getproducts = App\products::where('status', 1)->limit(4)->get(); @endphp
            
           
@foreach ($getproducts as $productlist)
<div class="grid-sizer col-lg-3 col-md-3 grid-item mb-4">
    <a href="{{route('products.list',$productlist->id)}}">
        <div class="product-block grid-v1">
            <div class="hovereffect">
                @foreach (json_decode($productlist->image) as $key => $products)
                    @if($key == 0)    
                    <img class="img-responsive w-100" src="{{asset('public/images/products/'.$products)}}" height="400" alt="{{ $productlist->prod_name}}">
                    @endif
                @endforeach
                <div class="overlay"> 
                    <div class="social-icons">
                        <a href="#" class="card-hover">
                            <button id="btnFA" class="btn btn-primary btn-xs">
                                <i class="fa fa-shopping-cart"></i>
                                ADD TO CART
                            </button>
                        </a>
                        <a href="#" data-tip="Add to Wishlist">
                            <i class="fa fa-heart-o"></i>
                        </a>
                        <a href="" data-tip="Add to Cart">
                            <i class="fa fa-share-alt"></i>
                        </a>
                    </div>    
                </div>
                <div class="metas clearfix">
                    <div class="title-wrapper">
                        <h3 class="name">
                        <a href="#">{{$productlist->prod_name}}</a>
                        </h3>
                        <span class="price">
                            <span class="woocommerce-Price-amount amount">
                                <bdi>{{$productlist->price}}</bdi>
                            </span>
                        </span>
                    </div>
                </div>
            </div>    
        </div>
    </a>                   
</div>
@endforeach --}}



<section class="pb-5" style="background-color:#d2d2d2;padding-top:50px;padding-bottom: 50px; ">

    <h1 class="text-center" style="color: #fff;">Related Products</h1>
    <br>
    
    <div class="container-fluid text-center">
        <!-- Masonry grid -->
        <div class="gallery-wrapper effect-1" id="grid">
            @php $getproducts = App\products::where(['categories_id' => $list->categories_id,'status'=> 1 ])->limit(8)->get(); @endphp
            
            @foreach ($getproducts as $productlist)
            <div class="grid-sizer col-lg-3 col-md-3 grid-item mb-4">
                <a href="{{route('products.list',$productlist->id)}}">
                    <div class="product-block grid-v1"  style="background: #fff;border-radius: 15px;">
                        <div class="hovereffect">
                          

                            @foreach (json_decode($productlist->image) as $key => $products)
                                @if($key == 0)    
                                 <img class="img-responsive w-100 " height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                               @endif

                               @if($key == 1)    
                                 <img class="img-responsive w-100 hover" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                               @endif
                            @endforeach

                            <div class="overlay"> 
                                 
                                <div class="social-icons">
                                    <a href="#" class="card-hover">
                                        <button id="btnFA" class="btn btn-primary btn-xs">
                                            <i class="fa fa-shopping-cart"></i>
                                            ADD TO CART
                                        </button>
                                    </a>
                                    <a href="#" data-tip="Add to Wishlist">
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                    <a href="" data-tip="Add to Cart">
                                        <i class="fa fa-share-alt"></i>
                                    </a>
                                </div>    
                            </div>
                            <div class="metas clearfix">
                                <div class="title-wrapper">
                                    <h3 class="name">
                                    <a href="#">{{$productlist->prod_name}}</a>
                                    </h3>
                                    <span class="price">
                                        <span class="woocommerce-Price-amount amount">
                                            <bdi>SAR {{$productlist->price}}</bdi>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>    
                    </div>
                </a>                   
            </div>

            @endforeach

            
      
        </div>
    </div>
</section>


@endsection