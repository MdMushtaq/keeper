@extends('layouts.front-app')
@section('content')

<div class="container">
    <div class="py-5 text-center">
        <h2>Payment</h2>
        <p class="lead">Need Some Informtion"</p>
    </div>
    <div class="stepwizard col-md-offset-3">
    	<div class="stepwizard-row setup-panel">
      		<div class="stepwizard-step">
        		<a href="#step-1" type="button" class="btn in-active-step btn-circle">1</a>
        		<p>Account</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-2" type="button" class="btn btn-default btn-circle in-active-step" disabled="disabled">2</a>
        		<p>Address</p>
      		</div>
      		<div class="stepwizard-step">
        		<a href="#step-3" type="button" class="btn btn-default btn-circle btn-primary" disabled="disabled">3</a>
        		<p>Payment</p>
      		</div>
    	</div>
  	</div>
    <div class="row">
        <div class="col-md-4 order-md-2 mt-4 mb-4 border-gray p-0">
            <div class="payment">
            	<h4 class="mt-3 ml-4">Cart - item(s)</h4>
            </div>	
    		
    		<div class="col-md-12 mt-4 mb-4 border-bottom-img">
    			<div class="row product-container">
	        		<div class="col-md-4 col-sm-4 col-xs-4">
	            		<div class="product-img">
	                		<img src="https://saidaliah.me/9/uploads/images/full/a44db462fbb969d436f770286b871006.jpg" class="img-thumbnail rounded">
	                	</div>
	        		</div>
	        		<div class="col-md-8 col-sm-8 col-xs-8">
						<p class="product-text">Orange Blossom Water 125ml</p>
	            		<p>2 x   SR35.00</p>
	        		</div>
	        	</div>	
    		</div>

    		<div class="col-md-12 mt-4 mb-4 border-bottom-img">
    			<div class="row product-container">
	        		<div class="col-md-4 col-sm-4 col-xs-4">
	            		<div class="product-img">
	                		<img src="https://saidaliah.me/9/uploads/images/full/a44db462fbb969d436f770286b871006.jpg" class="img-thumbnail rounded">
	                	</div>
	        		</div>
	        		<div class="col-md-8 col-sm-8 col-xs-8">
						<p class="product-text">Orange Blossom Water 125ml</p>
	            		<p>2 x   SR35.00</p>
	        		</div>
	        	</div>	
    		</div>

    		<div class="payment">
            	<h4 class="mt-3 ml-4">Total</h4>
            </div>

    		<div class="col-md-12 mt-4 mb-4 border-bottom-img">
    			<div class="row product-container">
	        		<div class="col-md-12 col-sm-12 col-xs-12">
	            		<table class="table">
                			<tbody>
                				<tr>
                					<td> Subtotal:</td>
                					<td class="text-right">SR805.00</td>
                				</tr>
  								<tr>
  									<td>
  										<strong>Total:</strong>
  									</td>
  									<td class="text-right">
  										<strong>SR845.25</strong>
  									</td>
  								</tr>
                			</tbody>
            			</table>
	        		</div>
	        	</div>	
    		</div>
        </div>

        <div class="col-md-8 order-md-1 mt-4 mb-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="payment">
                        <h4 class="mt-3 ml-4">Bank Transfer</h4>
                    </div>
                    <h4 class="text-center mt-3">Bank Transfer Instructions</h4>
                    <p class="text-center"><b>Please transfer the total amount to the following bank account.</b></p>
                    <div class="well well-sm">
                      <p><br>
                         Account Name : <br>
                         Account Number: <br>
                         IBAN : <br>
                         <br>
                         <br>
                         <br>
                         Account Name : <br>
                         Account Number: <br>
                         IBAN : <br>
                      </p>
                      <p>Your order will not ship until we receive payment.</p>
                    </div>
                </div>
            </div>
        	<div class="row mt-3">
                <div class="col-md-6">
                    <div class="payment">
                        <h4 class="mt-3 ml-4">Payment Methods</h4>
                    </div>
                    <div class="shipping">
                        <p>Bank Transfer</p>
                        <p>Cash on Delivery</p>
                        <p>
                            <img class="img-fluid cc-img payment-img" src="https://saidaliah.me/9/uploads/offer/visa-logo.jpg" />
                        </p>
                    </div>
                    <div class="custom-control custom-checkbox mt-3 mb-3">
                        <input type="checkbox" class="custom-control-input" id="agree-terms">
                        <label class="custom-control-label" for="agree-terms">
                            <a href="#" target="_blank">I have read and agree to the Terms & Conditions</a>
                        </label>
					</div>
					
                </div>
                <div class="col-md-6">
                    <div class="payment">
                        <h4 class="mt-3 ml-4">Order Comment</h4>
                    </div>
                    <div class="shipping p-0">
                        <textarea name="comment" class="form-control form-t" rows="4" placeholder=" Enter your Comment..."></textarea>
                    </div>    
                </div>
            </div>
        </div>
    </div>

    
</div>

@endsection