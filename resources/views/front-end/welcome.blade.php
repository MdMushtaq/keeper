@extends('layouts.front-app')

@section('content')
<style>
    .mobile-social-share {
        background: none repeat scroll 0 0 #EEEEEE;
        display: block !important;
        min-height: 70px !important;
        margin: 50px 0;
    }

    body {
        color: #777777;
        font-family: "Lato","Helvetica Neue","Arial","Helvetica",sans-serif;
        font-size: 13px;
        line-height: 19.5px;
    }


    .mobile-social-share h3 {
        color: inherit;
        float: left;
        font-size: 15px;
        line-height: 20px;
        margin: 25px 25px 0 25px;
    }

    .share-group {
        float: right;
        margin: 18px 25px 0 0;
    }

    .btn-group {
        display: inline-block;
        font-size: 0;
        position: relative;
        vertical-align: middle;
        white-space: nowrap;
    }

    .mobile-social-share ul {
        float: right;
        list-style: none outside none;
        margin: 0;
        min-width: 61px;
        padding: 0;
    }

    .share {
        min-width: 17px;
    }

    .mobile-social-share li {
        display: block;
        font-size: 18px;
        list-style: none outside none;
        margin-bottom: 3px;
        margin-left: 4px;
        margin-top: 3px;
    }

    .btn-share {
        background-color: #BEBEBE;
        border-color: #CCCCCC;
        color: #333333;
    }

    .btn-twitter {
        background-color: #3399CC !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-facebook {
        background-color: #3D5B96 !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-facebook {
        background-color: #3D5B96 !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-google {
        background-color: #DD3F34 !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-linkedin {
        background-color: #1884BB !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-pinterest {
        background-color: #CC1E2D !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-mail {
        background-color: #FFC90E !important;
        width: 40px;
        color:#FFFFFF!important;
    }

    .caret {
        border-left: 4px solid rgba(0, 0, 0, 0);
        border-right: 4px solid rgba(0, 0, 0, 0);
        border-top: 4px solid;
        display: inline-block;
        height: 0;
        margin-left: 2px;
        vertical-align: middle;
        width: 0;
    }

    #socialShare {
        max-width:59px;
        margin-bottom:7px;
        margin-top: 7px;
    }

    #socialShare > a{
        padding: 6px 10px 6px 10px;
    }

    @media (max-width : 320px) {
        #socialHolder{
            padding-left:5px;
            padding-right:5px;
        }

        .mobile-social-share h3 {
            margin-left: 0;
            margin-right: 0;
        }

        #socialShare{
            margin-left:5px;
            margin-right:5px;
        }

        .mobile-social-share h3 {
            font-size: 15px;
        }
    }

    @media (max-width : 238px) {
        .mobile-social-share h3 {
            font-size: 12px;
        }
    }


</style>
@if ($message = Session::get('success'))
<script>
    alert("Product Url has been sent");
</script>
@endif

@if(request()->is('/') || request()->segment(1) == 'price')

    
    <?php
    $min = 0;
    $max = 1000;

    ?>

    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-md-12">
                <!-- Main Slider -->
                <div class="position-relative">
                    <div id="main-slider" class="carousel slide {{\Session::get('locale') == 'ar' ? 'rtl' : ''}} data-ride="carousel">
                        <!-- Indicators -->
                        <ul class="carousel-indicators">
                            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#main-slider" data-slide-to="1"></li>
                            <li data-target="#main-slider" data-slide-to="2"></li>
                            <li data-target="#main-slider" data-slide-to="3"></li>
                            <li data-target="#main-slider" data-slide-to="4"></li>
                            
                        </ul>
                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{asset('public/frontend/img/slider/banner.png')}}" class="img-fluid" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('public/frontend/img/slider/banner-1.png')}}" class="img-fluid" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('public/frontend/img/slider/banner-2.png')}}" class="img-fluid" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="{{asset('public/frontend/img/slider/banner-3.png')}}" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Main Slider --> 
            </div>
        </div>
    </div>

    @if(\Session::get('locale') == 'ar')
    <div class="container-fluid brand-logo">
        <div class="row">
            <!-- Brand Logos Slider -->
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/9.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/8.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/7.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/6.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/5.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/4.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/3.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/2.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/1.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <!-- End Brand Logos Slider -->  
        </div>
    </div>
    
    @else
    <div class="container-fluid brand-logo">
        <div class="row">
            <!-- Brand Logos Slider -->
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/1.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/2.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/3.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/4.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/5.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/6.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/7.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/8.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <div class="col">
                <div class="brands_item d-flex flex-column justify-content-center">
                    <a href="#">
                        <img src="{{asset('public/frontend/img/brands/9.png')}}" alt="">
                    </a>    
                </div> 
            </div>
            <!-- End Brand Logos Slider -->  
        </div>
    </div>
    @endif
    <div class="container-fluid mt-5 mb-5">
        <div class="row">
            <div class="col-md-2 text-center">
                <a class="btn btn-primary btn-lg" href="{{url('/')}}">{{ trans('welcome.View All')}}</a>
            </div>
            {{-- <div class="col text-center">
                <a class="btn btn-primary btn-lg" href="#">Special Offer</a>
            </div> --}}
            <div class="col-md-2 text-center">
            <a class="btn btn-primary btn-lg" href="{{route('new.product')}}">{{ trans('welcome.New Products')}}</a>
            </div>
            <div class="col-md-2 text-center">
                <a class="btn btn-primary btn-lg" href="{{route('popular.product')}}">{{ trans('welcome.Popular Products')}}</a>
            </div>
            <div class="col-md-2 text-center">
                <a class="btn btn-primary btn-lg" href="{{route('topsell.product')}}">{{ trans('welcome.Top Selling Products')}}</a>
            </div>

        
            <div class="col-md-4">
                <div class="form-price-range-filter">
                    <form method="post" action="{{route('price.filter')}}">
                        @csrf
                    <div>
                        <input type="" id="min" name="min_price" value="<?php echo $min; ?>">
                        <div id="slider-range"></div>
                        <input type="" id="max" name="max_price" value="<?php echo $max; ?>">
                    </div>
                    <div>
                        <input type="submit" value="{{ trans('welcome.Filter Product')}}" class="btn btn-primary mt-3">
                    </div>
                    </form>
                </div>
            </div>

            {{-- <form action="{{route('price.filter')}}" method="POST">
                @csrf
                <div class="form-group">
                    <input type="text" name="lowprice" class="form-controller">
                </div>
                <div class="form-group">
                    <input type="text" name="highprice" class="form-controller">
                </div>

                <button type="submit" >Filter</button>
            </form> --}}
        </div>


    </div>
    
@endif

@if(request()->segment(1) == 'category')
<div class="container-fluid p-0">
    <div class="row">
        <div class="col-md-12">
            <!-- Main Slider -->
            <div class="position-relative">
                <div id="main-slider" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#main-slider" data-slide-to="1"></li>

                        
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        @foreach (json_decode($categoriesImage->cat_img) as $key => $imageslid)
                        @if($key == 0)
                        <div class="carousel-item active">
                            <img src="{{asset('public/images/categories/'.$imageslid)}}" class="img-fluid" alt="">
                        </div>
                        @endif
                        
                        @if($key == 1)
                        <div class="carousel-item">
                            <img src="{{asset('public/images/categories/'.$imageslid)}}" class="img-fluid" alt="">
                        </div>
                        @endif

                        @endforeach
                    </div>
                </div>
            </div>
            <!-- End Main Slider --> 
        </div>
    </div>
</div>

@else

@endif

  

<section class="pb-5" style="background-image:url({{asset('public/frontend/img/background.jpg')}});background-repeat:repeat;padding-top:50px;padding-bottom: 50px; ">
    <form action="{{ url('/compare-page') }}" method="POST">
        @csrf
    <button type="submit" class="btn btn-primary" style="margin-left: 20px">Compare</button>

    <div class="masonry">
        @foreach ($getproducts as $productlist)
        <div class="item">
            <a href="{{route('products.list', $productlist->id)}}">
                <div class="product-block grid-v1">
                    <div class="hovereffect" style="display: block">
                        @foreach (json_decode($productlist->image) as $key => $products)
                            @if($key == 0)    
                            <img class="img-responsive w-100 " height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif

                            @if($key == 1)    
                                <img class="img-responsive w-100 hover" height="{{$productlist->img_height}}"  src="{{asset('public/images/products/'.$products)}}" alt="{{ $productlist->prod_name}}">
                            @endif
                        @endforeach

                        <div class="metas clearfix">
                            <div class="title-wrapper">
                                <h3 class="name">
                                    @if(\Session::get('locale') == 'ar')
                                    <a href="#">{{$productlist->arabic_name}}</a>
                                    @else
                                    <a href="#">{{$productlist->prod_name}}</a>
                                    @endif
                                </h3>
                                <span class="price">
                                    <span class="woocommerce-Price-amount amount">
                                        <bdi>{{ trans('welcome.SAR')}} {{$productlist->price}}</bdi>
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="overlay"> 
                            <div class="social-icons" style="display: inline; max-width: 20px;">
                                <a href="#" class="card-hover">
                                    <button id="btnFA" class="btn btn-primary btn-xs">
                                        <i class="fa fa-shopping-cart"></i>
                                        {{ trans('welcome.ADD TO CART')}}
                                    </button>
                                </a>
                                <a href="#" data-tip="Add to Wishlist">
                                    <i class="fa fa-heart-o"></i>
                                </a>
<!--                                <a href="" data-tip="Add to Cart">-->
<!--                                    <i class="fa fa-share-alt"></i>-->
<!--                                </a>-->

                            </div>
                            <div id="socialHolder" style="display: inline; width: 10px;" >
                                <div id="socialShare" class="btn-group share-group">
                                    <a data-toggle="dropdown" class="btn btn-info">
                                        <i class="fa fa-share-alt fa-inverse"></i>
                                    </a>
<!--                                    <button href="#" data-toggle="dropdown" class="btn btn-info dropdown-toggle share">-->
<!--                                        <span class="caret"></span>-->
<!--                                    </button>-->
                                    <ul class="dropdown-menu" style="margin-left: -25px">
                                        <li>
                                            <a href="https://api.whatsapp.com/send?text={{route('products.list', $productlist->id)}}"
                                               class="btn btn-whatsapp" data-placement="left" style="background-color: green;">
                                                <i class="fa fa-whatsapp" style="color: white"></i>
                                            </a>
                                            <a onclick="addproducturl('{{route('products.list', $productlist->id)}}')" data-original-title="Email" rel="tooltip" class="btn btn-mail" data-placement="left" data-toggle="modal" data-target="#exampleModal">
                                                <i class="fa fa-envelope"></i>
                                            </a>
                                        </li>



                                    </ul>
                                </div>

                            </div>
                            <div>
                                <span>Compare</span>
                                <input type="checkbox" name="productids[]" value="{{ $productlist->id }}" class="comparisioncheckbox">
                            </div>
                        </div>

                    </div>    
                </div>
            </a>                   
        </div>
        @endforeach
        
    </div>
    </form>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('/sendproductemail') }}" method="POST">
                @csrf
            <div class="modal-body">
                <div class="form-group">
                    <input type="hidden" id="producturl" name="producturl">
                    <label>Enter Email</label>
                    <input type="email" name="email" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-brimary">Send</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="removeproducturl()">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    var countChecks = 0;
    $(document).ready(function(){
        $('.comparisioncheckbox').change(function(){
            if(countChecks == 4)
            {
                $('.comparisioncheckbox').attr("disabled", true);
                alert('You can not compare more than four products');
            }
            else if(this.checked)
            {
                $(this).attr("class","comparisioncheckbox1");
                countChecks++;
            }
        });
    });

    function addproducturl(productUrl)
    {
        $('#producturl').val(productUrl);
    }

    function removeproducturl()
    {
        $('#producturl').val("");
    }
</script>
@endsection

