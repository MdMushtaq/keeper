<!DOCTYPE html>
<html lang="en">
<head>
    <title>Keeper</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--  Favicon Icon -->
    <link href="{{asset('public/frontend/img/favicon.png')}}" rel="shortcut icon" type="image/png">
    <!--  Font Awesome -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    @if(\Session::get('locale') == 'ar')

    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.1.3/css/bootstrap.min.css" integrity="sha384-Jt6Tol1A2P9JBesGeCxNrxkmRFSjWCBW1Af7CSQSKsfMVQCqnUVWhZzG0puJMCK6" crossorigin="anonymous">
   
    @else
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    @endif
    <!--  Bootstrap -->
    
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <!--  Flag Css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('public/frontend/css/custom.css')}}" media="screen" rel="stylesheet" type="text/css">

    <!--  Zoom Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/xzoom.css')}}" media="all" />
        
    <!--  Zoom Fancybox -->
    <link type="text/css" rel="stylesheet" media="all" href="{{asset('public/frontend/fancybox/source/jquery.fancybox.css')}}" />

    <!--  Magnific Popup -->
    <link type="text/css" rel="stylesheet" media="all" href="{{asset('public/frontend/magnific-popup/css/magnific-popup.css')}}" />
    
    <!--  Product Css -->
    <link href="{{asset('public/frontend/css/product-description.css')}}" media="screen" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/default.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/component.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/home.css')}}" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" media="all" />
    
    @if(\Session::get('locale') == 'ar')
        <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/rtl.css')}}">

    @endif 
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
    <body>
        <?php
        $min = 0;
        $max = 1000;
        
        ?>
        @if(\Session::get('locale') == 'ar')

        <header class="bg-primary container-fluid p-0">
            <div class="container overflow-hidden">
                <!-- Logo -->
                <div class="row justify-content-center align-items-center header">
                    <div class="d-none d-xl-block col-xl-2 col-sm-12">
                        <ul class="language-switcher">
                            <nav class="navbar navbar-dark py-0 d-none d-md-flex">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link text-white py-0" href="{{url('localization/en')}}">

                                          English
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </ul>
                        <div class="cart">
                            {{ trans('welcome.CART')}}

                            @php
                                $cartCount = 0;

                                if (auth()->check()) {
                                    $cartCount = \App\Cart::where(['user_id' => auth()->id(), 'status' => 0])->count();
                                }
                            @endphp

                            <a href="{{route('checkout.cart')}}" id="addednumbeofproducts" class="fa-stack fa-2x has-badge" data-count="{{$cartCount}}">
                            <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                            <i style="" class="fa fa-shopping-cart fa-stack-2x red-cart"></i>
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-xl-block col-xl-8 col-sm-12 p-0">
                        <!-- Top Navigation Bar -->
                        <div id="nav-top" class="py-1">
                            <nav class="navbar navbar-dark py-0 d-none d-md-flex">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link text-white px-2 py-0" href="#">
                                            <span>{{ trans('welcome.TOP BRANDS')}}</span>
                                            <img src="{{asset('public/frontend/img/3.png')}}" alt="">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-white py-0" href="#">
                                            <span>{{ trans('welcome.BEST DEALS')}}</span>
                                            <img src="{{asset('public/frontend/img/2.png')}}" alt="">
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-white py-0" href="#">
                                            <span>{{ trans('welcome.FREE AND ESAY RETURNS')}}</span>
                                            <img src="{{asset('public/frontend/img/1.png')}}" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- End Top Navigation Bar -->
                        <div class="row">
                            <div class="col-xl-3">
                                <ul class="nav sign-in">
                                    @guest
                                    @if (Route::has('register'))
                                    <li class="nav-item mt-1 text-white">
                                        <span><a class="text-white py-0" href="{{url('/register')}}">{{ trans('welcome.SIGN UP')}}</a> / </span>
                                        <span> <a class="text-white py-0" href="{{url('/login')}}">{{ trans('welcome.SIGN IN')}}</a></span>
                                        <i class="fa fa-user " aria-hidden="true"></i>
                                    </li>
                                    @endif
                                @else
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-white py-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ Auth::user()->name }}
                                        <i class="fa fa-caret-down text-right"></i>
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li>
                                            <a class="dropdown-item" href="{{route('customer.dashboard')}}">Dashboard</a>
                                        </li>
                                        <li>
                                            <form action="{{ route('logout') }}" method="POST">
                                                @csrf
                                                <button class="dropdown-item" type="submit">
                                                    {{ __('Logout') }}
                                                </button>
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @endguest
                                    {{-- <li class="nav-item mt-1">
                                        <a class="text-white py-0" href="#">
                                            <span>???</span>
                                            <span>/ ????? ??????</span>
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                        </a>
                                    </li> --}}
                                </ul>
                            </div>
                            <div class="col-xl-9">
                                <form class="form-inline my-lg-0 d-none d-md-block search-form" action="#" method="GET">
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <button class="btn btn-sm bg-senary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                        <input class="form-control bg-senary border-0 pl-3" name="query" type="search" placeholder="???" aria-label="???">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-2 col-sm-12 p-0">
                        <a href="{{url('/')}}">
                            <img src="{{asset('public/frontend/img/logo.png')}}" class="logo" alt="">
                        </a>
                    </div>
                </div>
                <!-- End Logo -->
            </div>
        </header>
        <div class="container">
            <div class="row">
                <!-- Primary Navigation Bar Desktop -->
                <nav class="navbar navbar-expand-md navbar-hover" id="main_navbar">
                    <button class="navbar-toggler border-0 py-4 collapsed shop-toggle" type="button" data-toggle="collapse" data-target="" aria-controls="" aria-expanded="" aria-label="">
                     <i class="fa fa-bars"></i> &nbsp;&nbsp;&nbsp;   {{ trans('welcome.SHOP MENU')}}
                    </button>

                    <div class="collapse navbar-collapse" id="main_navbar">
                        <ul class="navbar-nav main-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ trans('welcome.CONTACT US')}}
                                </a>
                            </li>
                            <li class="nav-item  dropdown dropdown-hover">
                                <a href="#" class="nav-link dropdown-toggle">
                                    {{ trans('welcome.HELP')}}
                                </a>
                            </li>
                            <li class="nav-item  dropdown dropdown-hover">
                                <a href="#" class="nav-link dropdown-toggle">
                                    {{ trans('welcome.ABOUT US')}}
                                </a>
                            </li>
                            <li class="nav-item  dropdown dropdown-hover">
                                <a href="{{ url('offers') }}" class="nav-link dropdown-toggle">
                                    {{ trans('welcome.SPECIAL OFFER')}}
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   <i class="fa fa-caret-down text-right"></i> {{ trans('welcome.PRODUCTS')}}
                                </a>
                                <ul class="dropdown-menu" >
                                    @php  $catagories = App\Categories::where('cat_status', 1)->get();  @endphp
                                    @foreach ($catagories as $item)

                                        <li >
                                            <a class="dropdown-item dropdown-toggle" href="{{route('category.list',$item->id)}}" >

                                                {{$item->cat_arabic_name}}

                                            </a>
                                            <ul class="dropdown-menu" >

                                                @php $subcat = App\subcategories::where(['status' => 1 ,'categories_id' => $item->id])->get(); @endphp

                                                @foreach ($subcat as $subitem)
                                                <li>

                                                    <a class="dropdown-item" href="{{route('subcategory.list',$subitem->id)}}">
                                                        {{$subitem->sub_cat_arabic_name}}
                                                    </a>

                                                </li>
                                                @endforeach

                                            </ul>
                                        </li>
                                    @endforeach

                                </ul>

                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    <i class="fa fa-caret-down text-right"></i>
                                    {{ trans('welcome.CATEEGORIES')}}
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li>
                                        <a class="dropdown-item" href="#">{{ trans('welcome.Electronics')}}</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                   </button>
                </nav>
                <!-- End Primary Navigation Bar Desktop -->
            </div>
        </div>
        @else

        {{-- english Header --}}

        <header class="bg-primary container-fluid p-0">
            <div class="container overflow-hidden">
                <!-- Logo -->
                <div class="row justify-content-center align-items-center header">
                    <div class="col-xl-2 col-sm-12 p-0">
                        <a href="{{url('/')}}">
                            <img src="{{asset('public/frontend/img/logo.png')}}" class="logo" alt="">
                        </a>
                    </div>
                    <div class="d-none d-xl-block col-xl-8 col-sm-12 p-0">
                        <!-- Top Navigation Bar -->
                        <div id="nav-top" class="py-1">
                            <nav class="navbar navbar-dark py-0 d-none d-md-flex">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link text-white py-0" href="#">
                                            <img src="{{asset('public/frontend/img/1.png')}}" alt=""> <span>{{ trans('welcome.FREE AND ESAY RETURNS')}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-white py-0" href="#">
                                            <img src="{{asset('public/frontend/img/2.png')}}" alt=""> <span>{{ trans('welcome.BEST DEALS')}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-white px-2 py-0" href="#">
                                            <img src="{{asset('public/frontend/img/3.png')}}" alt=""> <span>{{ trans('welcome.TOP BRANDS')}}</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- End Top Navigation Bar -->
                        <div class="row">
                            <div class="col-xl-9">
                                <form class="form-inline my-lg-0 d-none d-md-block search-form" action="#" method="GET">
                                    <div class="input-group">
                                        <input class="form-control bg-senary border-0 pl-3" name="query" type="search" placeholder="Search" aria-label="Search">
                                        <div class="input-group-append">
                                            <button class="btn btn-sm bg-senary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xl-3">
                                <ul class="nav sign-in">
                                   
                                    @guest
                                        @if (Route::has('register'))
                                        <li class="nav-item mt-1 text-white">
                                            
                                            <i class="fa fa-user " aria-hidden="true"></i> <span><a class="text-white py-0" href="{{url('/login')}}">{{ trans('welcome.SIGN IN')}}</a> / </span> <span><a class="text-white py-0" href="{{url('/register')}}">{{ trans('welcome.SIGN UP')}}</a></span>
                                            
                                        </li>
                                        @endif
                                    @else
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle text-white py-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ Auth::user()->name }} 
                                            <i class="fa fa-caret-down text-right"></i>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li>
                                                <a class="dropdown-item" href="{{route('customer.dashboard')}}">Dashboard</a>
                                            </li>
                                            <li>
                                                <form action="{{ route('logout') }}" method="POST">
                                                    @csrf
                                                    <button class="dropdown-item" type="submit">
                                                        {{ __('Logout') }}
                                                    </button>
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                    @endguest
                                </ul>
                            </div>
                        </div>    
                    </div>
                    <div class="d-none d-xl-block col-xl-2 col-sm-12">
                        <ul class="language-switcher">
                            @php $locale = session()->get('localization'); @endphp

                            <nav class="navbar navbar-dark py-0 d-none d-md-flex">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link text-white py-0" href="{{url('localization/ar')}}">
                                           
                                            العربية
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </ul>
                        <div class="cart">
                            {{ trans('welcome.CART')}}

                            @php 
                                $cartCount = 0;
                              
                                if (auth()->check()) {
                                    $cartCount = \App\Cart::where(['user_id' => auth()->id(), 'status' => 0])->count();
                                }
                            @endphp

                            <a href="{{route('checkout.cart')}}" id="addednumbeofproducts" class="fa-stack fa-2x has-badge" data-count="{{$cartCount}}">
                            <i class="fa fa-circle fa-stack-2x fa-inverse"></i>
                            <i style="" class="fa fa-shopping-cart fa-stack-2x red-cart"></i>
                            </a>
                        </div>    
                    </div>
                </div>
                <!-- End Logo -->
            </div>    
        </header>
        <div class="container">
            <div class="row">
                <!-- Primary Navigation Bar Desktop -->
                <nav class="navbar navbar-expand-md navbar-hover" id="main_navbar">
                   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                   </button>
                    <div class="collapse navbar-collapse" id="main_navbar">
                        <ul class="navbar-nav main-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ trans('welcome.CATEEGORIES')}}
                                    <i class="fa fa-caret-down text-right"></i>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li>
                                        <a class="dropdown-item" href="#">{{ trans('welcome.Electronics')}}</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">

                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ trans('welcome.PRODUCTS')}} <i class="fa fa-caret-down text-right"></i>

                                </a>
                                <ul class="dropdown-menu" >

                                    @php  $catagories = App\Categories::where('cat_status', 1)->get();  @endphp
                                    @foreach ($catagories as $item)
                                   
                                        <li >
                                            <a class="dropdown-item dropdown-toggle" href="{{route('category.list',$item->cat_name )}}" >
                                              {{$item->cat_name}}
                                            </a>
                                            <ul class="dropdown-menu" >

                                                @php $subcat = App\subcategories::where(['status' => 1 ,'categories_id' => $item->id])->get(); @endphp

                                                @foreach ($subcat as $subitem)
                                                <li>
                                                    <a class="dropdown-item " href="{{route('subcategory.list',$subitem->id)}}">{{$subitem->sub_cat_name}}</a>

                                                </li>
                                                @endforeach
                                            
                                            </ul>
                                        </li>
                                    @endforeach

                                </ul>
                               
                            </li>
                            <li class="nav-item  dropdown dropdown-hover">
                                <a href="{{ url('offers') }}" class="nav-link dropdown-toggle">
                                    {{ trans('welcome.SPECIAL OFFER')}}
                                </a>
                            </li>
                            <li class="nav-item  dropdown dropdown-hover">
                                <a href="#" class="nav-link dropdown-toggle">
                                    {{ trans('welcome.ABOUT US')}}
                                </a>
                            </li>
                            <li class="nav-item  dropdown dropdown-hover">
                                <a href="#" class="nav-link dropdown-toggle">
                                    {{ trans('welcome.HELP')}}
                                </a>
                            </li>
                            <li class="nav-item dropdown">

                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ trans('welcome.CONTACT US')}}

                                </a>
                            </li>
                        </ul>
                    </div>
                    <button class="navbar-toggler border-0 py-4 collapsed shop-toggle" type="button" data-toggle="collapse" data-target="" aria-controls="" aria-expanded="" aria-label="">
                        {{ trans('welcome.SHOP MENU')}} &nbsp;&nbsp;&nbsp;<i class="fa fa-bars"></i>
                    </button>
                </nav>
                <!-- End Primary Navigation Bar Desktop -->
            </div>
        </div>

        @endif

        <div>
            @yield('content')
        </div>
    </body>
<!-- Footer -->    
<footer class="footer-bg">
    <div class="container">
        <div class="row justify-content-center align-items-center">            
            <div class="col-md-2">
                <a href="index.php">
                    <img src="{{asset('public/frontend/img/logo.png')}}" class="logo" alt="">
                </a>
            </div>

            <div class="col-md-6">
                <form action="{{ url('/subscribe') }}" class="form-inline" style="margin-left: 51px;" method="POST">
                    @csrf

                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Enter Newsletter Email">
                    </div>
                    <button type="submit" class="btn btn-default">Subscribe</button>
                </form>
                <nav class="navbar navbar-expand-md" id="footer_navbar">
                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">ABOUT US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">NEWS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ITEMS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">FAQ'S</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">PRIVACY</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONTACT US</a>
                        </li>
                    </ul>
                </nav>
            </div> 
            <div class="col-md-3">
               <ul class="list-unstyled list-inline text-center">
                 <li class="list-inline-item">
                    <a class="btn-floating btn-gplus mx-1">
                      <i class="fa fa-youtube-play"> </i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn-floating btn-fb mx-1">
                      <i class="fa fa-facebook-f"> </i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn-floating btn-tw mx-1">
                      <i class="fa fa-twitter"> </i>
                    </a>
                  </li>
                </ul>
                <!-- Social buttons -->
            </div>  
        </div>
    </div>
</footer>
<nav id="post-footer" class="navbar navbar-dark bg-gray small">
    <div class="navbar-text">
        © Copyright by Technosouq 2020
    </div>
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link text-white" href="https://www.jeddahsoft.org/" target="_blank">Power by Jeddahsoft</a>
        </li>
    </ul>
</nav>
<button id="btn-top" title="Go to top" class="btn bg-primary shadow text-white">
    <i class="fa fa-arrow-up"></i>
</button>
@if($message = Session::get('successfulsubsciption'))
<script>
    alert("Email Subscribed for Newsletter");
</script>
@endif
<script src="//code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="//res.cloudinary.com/dxfq3iotg/raw/upload/v1565190285/Scripts/xzoom.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="//unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js"></script>

<script type="text/javascript" src="{{asset('public/frontend/js/jquery.countdown.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/js/bootnavbar.js')}}" ></script>
<script type="text/javascript" src="{{asset('public/frontend/js/custom.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/js/modernizr.custom.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/js/AnimOnScroll.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/dist/xzoom.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/fancybox/source/jquery.fancybox.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/magnific-popup/js/magnific-popup.js')}}"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
 
    $(document).ready(function() {
   		$(function () {
		    // Initate masonry grid
		    var $grid = $('.gallery-wrapper').masonry({
		        temSelector: '.grid-item',
		        columnWidth: '.grid-sizer',
		        percentPosition: true,
		    });
		    // Initate imagesLoaded
		    $grid.imagesLoaded().progress( function() {
		        $grid.masonry('layout');
		    });
    
		});

		new AnimOnScroll( document.getElementById( 'grid' ), {
	        minDuration : 0.4,
	        maxDuration : 0.7,
	        viewportFactor : 0.2
	    } );
    });
    
    $(function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ '<?php echo $min; ?>', '<?php echo $max; ?>' ],
            slide: function( event, ui ) {
            $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            $( "#min" ).val(ui.values[ 0 ]);
            $( "#max" ).val(ui.values[ 1 ]);
        }
        });
        $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
        " - $" + $( "#slider-range" ).slider( "values", 1 ) );
     });

</script>



 </body>
</html>