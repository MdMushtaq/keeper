@extends('layouts.admin-app')
@section('content')

  @if (\Session::has('success'))
   <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
   </div>
   <br />

   @elseif(\Session::has('danger'))
   <div class="alert alert-danger">
      <p>{{ \Session::get('danger') }}</p>
   </div>
   <br />
   @endif 

<div class="page-header">
	<h1>Brands</h1>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                &nbsp;
            </div>
            <div class="col-md-8">
                <form action="#" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                	<div class="form-group"></div>
	                <div class="form-group">
	                    <input type="text" class="form-control" name="term" value="" placeholder="">
	                </div>
	                <button class="btn btn-default" name="submit" value="search">Search</button>
	                <a class="btn btn-default" href="#">Reset</a>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="text-right form-group">
    <a class="btn btn-primary" style="font-weight:normal;" href="{{route('add.brands')}}">
    	<i class="icon-plus"></i>  Add New Brand
    </a>
    <a class="btn btn-danger" style="font-weight:normal;" href="{{ route('brands.trash') }}">
        <i class="icon-times"></i> Trash Brand
    </a>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Arabic Name</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($brands as $brand)
        <tr>
        <td>{{$brand->brands_name}}</td>
            <td>{{$brand->brands_arb_name}}</td>
            <td>{{$brand->brands_status == 1 ? 'Enable' : 'Disable'}}</td>
            <td class="text-right">
                <div class="btn-group">
                <a class="btn btn-default" href="{{route('brands.edit',$brand->id)}}"><i class="icon-pencil"></i></a>

                @if( $brand->brands_status == '0')
                <a class="btn btn-danger" href="{{ route('brnads.delete',$brand->id) }}" onclick="return confirm('are you sure?')"><i class="icon-times"></i></a>
               @endif
            </div>
            </td>
        </tr>
        @endforeach
        
      
    </tbody>
</table>
@endsection