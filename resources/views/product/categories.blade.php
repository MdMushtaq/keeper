@extends('layouts.admin-app')
@section('content')


    @if (\Session::has('success'))
    <div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
    </div>
    <br />

    @elseif(\Session::has('danger'))
    <div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
    </div>
    <br />
    @endif

        <div class="page-header"><h1>Categories</h1></div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        &nbsp;
                    </div>
                    <div class="col-md-8">
                        <form action="#" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">

                            <div class="form-group">
                                <input type="text" class="form-control" name="term" value="" placeholder="">
                            </div>
                            <button class="btn btn-default" name="submit" value="search">Search</button>
                            <a class="btn btn-default" href="#">Reset</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div style="text-align:right">
            @can('Categories.Create')
            <a class="btn btn-primary" href="{{ url("admin/categories/form") }}"><i class="icon-plus"></i> Add New Category</a>
            @endcan
            @can('Categories.Delete')
            <a class="btn btn-danger" href="{{ route('trash.categories') }}"><i class="icon-reset"></i> Trash Category</a>
            @endcan
        </div>

        <table class="table table-striped">
            <thead>
            <tr>
                <!--<th><i class="icon-eye-slash"></i></th>-->
                <th>MainCategory name</th>
                <th>MainCatgory Arabic Name </th>
                <th>Category name</th>
                <th>Catgory Arabic Name </th>
                <th>Status</th>
                @can('Categories.Edit')
                    <th>Action</th>
                @endcan

            </tr>
            </thead>
            <tbody>

            @foreach ($categories as $category)
                @php $maincategory = App\MainCategory::where('id',$category->miancategory_id)->first(); @endphp
                 <tr>
                <!--<td></td>-->
                <td>{{$maincategory->name}}</td>
                <td>{{$maincategory->arabic_name}}</td>
                <td>{{$category->cat_name}}</td>
                <td>{{$category->cat_arabic_name}}</td>
                <td>{{ $category->cat_status == '0' ? 'Disable' : 'Enable'}}</td>

                @can('Categories.Update')
                <td >
                    <div class="btn-group">
                        <a class="btn btn-default" href="{{route('edit.categories',$category->id)}}"><i class="icon-pencil"></i></a>

                        @if( $category->cat_status == '0')

                            <a class="btn btn-danger" href="{{route('delete.categories',$category->id)}}" onclick="return confirm('are you sure you wan to delete?')"><i class="icon-times"></i></a>
                        @endif
                    </div>
                </td>
                @endcan
            </tr>
            @endforeach


            </tbody>
        </table>

@endsection
