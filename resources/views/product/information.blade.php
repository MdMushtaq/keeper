@extends('layouts.admin-app')
@section('content')

<div class="page-header">
    <h1>Pages</h1>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                     &nbsp;
                </div>
                <div class="col-md-8">
                    <form action="" class="form-inline form-group" style="float:right" method="post" accept-charset="utf-8">
                        <div class="form-group">
                            <input type="text" class="form-control" name="term" value="" placeholder="">
                        </div>
                        <button class="btn btn-default" name="submit" value="search">Search</button>
                        <a class="btn btn-default" href="#">Reset</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div style="text-align:right">
        <a class="btn btn-primary" href="{{ url('admin/pages/add') }}">
            <i class="icon-plus"></i>
            Add New Page
        </a>
        <a class="btn btn-primary" href="{{ url('admin/pages/addlink') }}">
            <i class="icon-plus"></i> Add New Link
        </a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th style="max-width:20px;"></th>
                <th>Title</th>
                <th>Arabic Title</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach($pages as $page)
        {
            ?>
            <tr>
                <td style="width:20px;"></td>
                <td><?= $page->title; ?></td>
                <td><?= $page->arabic_title; ?></td>
                <td class="text-right">
                    <div class="btn-group">
                        <a class="btn btn-default" href="#">
                            <i class="icon-pencil"></i>
                        </a>
                        <a class="btn btn-default" href="#" target="_blank">
                            <i class="icon-link"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php
        }
        ?>


        </tbody>
    </table>

@endsection