@extends('layouts.admin-app')
@section('content')

 @if(count($errors))
 	<div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.
        <br/>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

    <div class="container">

        <div class="page-header"><h1>Main Category Form</h1></div>
        <form action="{{route('main-categories.create')}}" method="post" accept-charset="utf-8">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="name">MainCategory Name</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">MainCategory Arabic Name</label>
                                <input type="text" name="arabic_name" required class="form-control" lang="ar" dir="rtl">
                               
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Status</label>
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="1">Enabled</option>
                                    <option value="0">Disabled</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
               
            </div>


        </form>
    </div>

@endsection
