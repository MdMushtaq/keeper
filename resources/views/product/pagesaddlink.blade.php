@extends('layouts.admin-app')
@section('content')
<div class="container">
    <script type="text/javascript">
        function areyousure(){
            return confirm('Are you sure you want to delete this category?');
        }
    </script>
    <style type="text/css">
        .pagination {
            margin:0px;
        }
    </style>
    <div class="page-header">
        <h1>Add New Link</h1>
    </div>
    <form action="{{ url('admin/pages/addlinkpost') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="menu_title">Title </label>
            <input type="text" name="title" value="" class="form-control">
        </div>
        <div class="form-group">
            <label for="arabic_menu_title">Arabic Title </label>
            <input type="text" name="arabic_title" value="" class="form-control">
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="url">URL</label>
                    <input type="text" name="url" value="" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>&nbsp;</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="new_window" value="1">
                            Open this link in a new window
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="sequence">Parent</label>
                    <select name="parent_id" class="form-control">
                        <option value="0" selected="selected">Top Level</option>
                        <option value="6"> CAREERS</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="sequence">Sequence</label>
                    <input type="text" name="sequence" value="0" class="form-control">
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                <div class="col-md-2">

                </div>
            </div>
        </div>
    </form>

    <hr>
    <footer></footer>
</div>
@endsection
