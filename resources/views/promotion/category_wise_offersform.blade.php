@extends('layouts.admin-app')
@section('content')

<div class="container">
    <script type="text/javascript">
        function areyousure(){
            return confirm('Are you sure you want to delete this category?');
        }
    </script>
    <style type="text/css">
        .pagination {
            margin:0px;
        }
    </style>
    <div class="page-header">
        <h1>Add Category Wise Offers &amp; Promotions</h1>
    </div>
    <form action="{{ url('/admin/category_wise_offers/formpost') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="offer_name">Offer Name</label>
                    <input type="text" name="offer_name" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="start_date">Enable On (UTC)</label>
                    <input type="text" name="start_date" value="" class="form_datetime form-control start_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="end_date">Disable On (UTC)</label>
                    <input type="text" name="end_date" value="" class="form_datetime form-control end_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="reduction_amount">Reduction Amount</label>
                    <div class="row">
                        <div class="col-md-6">
                            <select name="reduction_type" class="form-control">
                                <option value="percent">Percentage</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="reduction_amount" value="" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <div class="form-group">
                        <select name="enabled_1" class="form-control">
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-1 well pull-right">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Select MainCategory</label>
                            <select class="form-control maincategory" name="maincategory_id" id="maincategory" >
                                <option value="">SELECT</option>
                                @foreach ($maincategory as $maincategortitem)
                                <option value="{{$maincategortitem->id}}">{{$maincategortitem->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Select Category</label>
                            <select class="form-control primary_category" name="primary_category" id="primary_category" required>
                                <option value="">SELECT</option>
                                <?php
                                foreach($categories as $category)
                                {
                                    ?>
                                    <option value="<?= $category->id; ?>"><?= $category->cat_name; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="col-md-2"></div>
        </div>
    </form>

</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#maincategory").change(function(){
        var $maincatId = this.value;

        $.ajax({
            type: "POST",
            url: "{{ url('admin/getcategories') }}",
            data: {maincategoryid:$maincatId},
            success: function(result)
            {
                $('#primary_category').html(result);
            }
        });
    });

    $(document).ready(function(){
        $("#primary_category").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#secondary_category').html(result);
                }
            });

        });
    });
</script>
@endsection