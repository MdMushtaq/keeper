@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Edit Product Wise % Discount</h1>
</div>
<form action="{{ url('admin/product_offers/formpostedit') }}" method="post" accept-charset="utf-8" id="form1">
    {{ csrf_field() }}
    <input type="hidden" value="<?= $productwisediscount->id; ?>" name="productwisediscountid">
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label for="offer_name">Offer Name</label>
                <input type="text" name="offer_name" value="<?= $productwisediscount->offer_name; ?>" class="form-control">

            </div>
            <div class="form-group">
                <label for="start_date">Enable On (UTC)</label>
                <input type="text" name="start_date" value="<?= $productwisediscount->enable_on_utc; ?>" class="form_datetime form-control start_date" readonly="true">
            </div>
            <div class="form-group">
                <label for="end_date">Disable On (UTC)</label>
                <input type="text" name="end_date" value="<?= $productwisediscount->disable_on_utc; ?>" class="form_datetime form-control end_date" readonly="true">
            </div>
            <div class="form-group">
                <label for="reduction_amount">Reduction Amount</label>
                <div class="row">
                    <div class="col-md-6">
                        <select name="reduction_type" class="form-control">
                            <option value="<?= $productwisediscount->reduction_type; ?>" selected><?= $productwisediscount->reduction_type; ?></option>
                            <option value="percent">Percentage</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="reduction_amount" value="<?= $productwisediscount->percentage; ?>" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Status</label>
                <div class="form-group">
                    <select name="enabled_1" class="form-control">
                        <option value="1" {{$productwisediscount->status == 1 ? 'selected' : ''}}>Enabled</option>
                        <option value="0" {{$productwisediscount->status == 0 ? 'selected' : ''}}>Disabled</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-md-offset-1 well pull-right">
<!--            <select name="whole_order_coupon" id="gc_coupon_appliesto_fields" class="form-control">-->
<!--                <option value="" selected="selected">Apply offer to Select Items</option>-->
<!--            </select>-->
<!--            <div id="gc_coupon_products">-->
<!--                <table class="table" width="100%" border="0" style="margin-top:10px;" cellspacing="5" cellpadding="0">-->
<!--                    <tbody id="product_items_container"></tbody>-->
<!--                </table>-->
<!---->
<!--                <div class="alert alert-info" style="text-align:center;">-->
<!--                    <strong>Product already added in other offers are not visible here !</strong>-->
<!--                </div>-->
<!---->
<!--                <div class="form-group">-->
<!--                    <input class="form-control" type="text" id="product_search" placeholder="Product search">-->
<!--                </div>-->
<!--                <div class="form-group">-->
<!--                    <select class="form-control" id="product_list" size="5" style="margin:0px;"></select>-->
<!--                </div>-->
<!--                <div class="form-group">-->
<!--                    <a href="#" onclick="" class="btn btn-primary" title="Add Product">Add Product</a>-->
<!--                </div>-->
<!--            </div>-->
            <div class="row">
                <div class="col-md-4">
                    <label>Select Category</label>
                    <select class="form-control primary_category" name="primary_category" id="primary_category" required>
                        <option value="">SELECT</option>
                        <?php
                        foreach($categories as $category)
                        {
                            ?>
                            <option value="<?= $category->id; ?>"><?= $category->cat_name; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Select Sub Category</label>
                    <select class="form-control secondary_category" name="secondary_category" id="secondary_category" required>
                        <option value="">SELECT</option>

                    </select>
                </div>



                <div class="col-md-4">
                    <label>Select Products</label>
                    <select id="products" name="products" class="form-control"  style="width: 100%" >
                        <option value="">None</option>
                    </select>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <a href="#" onclick="addproduct()" class="btn btn-primary" title="Add Product">Add Product</a>
                    </div>
                    <div class="col-md-12" id="selectedproducts">
                        <input type="hidden" name="numberofproducts" id="numberofproducts" value="">
                        <h3>Selected Products</h3>
                        <?php
                        $count = 1;
                        foreach($promotionselectedproducts as $promotionselectedproduct)
                        {
                            ?>
                            <div class="col-md-12">
                            <p style="display: inline"><?= $count; ?>. <?= $promotionselectedproduct->prod_name; ?></p>
                            <input type="hidden" name="product<?= $count; ?>" value="<?= $promotionselectedproduct->id; ?>">
                            <a style="padding: 2px 8px; cursor: pointer; float: right; background-color: red;" onclick="removeproduct(<?= $promotionselectedproduct->id; ?>)"><i class="icon-times"></i></a>
                            </div>
                        <?php
                            $count++;
                        }
                        ?>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
        <div class="col-md-2"></div>
    </div>
</form>


<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#primary_category").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#secondary_category').html(result);
                }
            });

        });

        $("#secondary_category").change(function()
        {
            var $catId = this.value;
            var promotionid = "<?php echo $productwisediscount->id; ?>";

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategoryproducts1') }}",
                data: {subcategoryid:$catId, promotionid:promotionid},
                success: function(result)
                {
                    $('#products').html(result);
                }
            });
        });
    });

    var productcount = "<?php echo $count; ?>";

    var selectedproductids = [];

    function addproduct()
    {
        var product = $("#products option:selected");
        var productvalue = $("#products option:selected").val();
//        product.remove();
        var productHtml = product.html();


        var productfound = false;
        for(var a = 0; a < selectedproductids.length; a++)
        {
            if(productvalue == selectedproductids[a])
            {
                alert('Product Added');
                return false;
            }
        }


        productHtml = productcount+". "+productHtml;

//        selectedproducts

        var nodediv = document.createElement("div");
        nodediv.setAttribute("class","col-md-12");
        nodediv.setAttribute("id","prdouctdivid"+productvalue);

        var node = document.createElement("p");                 // Create a <li> node
        var textnode = document.createTextNode(productHtml);
        node.appendChild(textnode);
        node.setAttribute("style","display: inline");

        var node1 = document.createElement("input");                 // Create a <li> node
//        var textnode = document.createTextNode(productHtml);
        node1.setAttribute("type","hidden");
        node1.setAttribute("name","product"+productcount);
        node1.setAttribute("value",productvalue);

        nodediv.appendChild(node);
        nodediv.appendChild(node1);


        var atag = document.createElement("a");
        atag.setAttribute("style","padding: 2px 8px;cursor: pointer; float: right; background-color: red");
        var buttonfunction = "removeproduct("+productvalue+")";
        atag.setAttribute("onclick",buttonfunction);


        var itag =  document.createElement("i");
        itag.setAttribute("class","icon-times");
        atag.appendChild(itag);
        nodediv.appendChild(atag);


        document.getElementById("selectedproducts").appendChild(nodediv);
//        document.getElementById("selectedproducts").appendChild(node1);

        document.getElementById("numberofproducts").value = productcount;

        productcount++;

        selectedproductids.push(productvalue);
    }

//    alert('234');



</script>
<script>
    function removeproduct(id)
    {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/productpromotionremoveproduct') }}",
                data: {productid:id},
                success: function(result)
                {
                location.reload();
                }
            });
    }

</script>
@endsection