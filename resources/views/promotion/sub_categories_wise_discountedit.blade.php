@extends('layouts.admin-app')
@section('content')
<div class="container">
    <script type="text/javascript">
        function areyousure(){
            return confirm('Are you sure you want to delete this category?');
        }
    </script>
    <style type="text/css">
        .pagination {
            margin:0px;
        }
    </style>
    <div class="page-header">
        <h1>Add Sub Category Wise Offers &amp; Promotions</h1>
    </div>
    <form action="{{ url('/admin/sub_category_wise_offers/formpostedit') }}" method="post" accept-charset="utf-8">
        {{ csrf_field() }}
        <input type="hidden" value="<?= $category_wise_discounts->id; ?>" name="categorywisediscountid">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="offer_name">Offer Name</label>
                    <input type="text" name="offer_name" value="<?= $category_wise_discounts->offer_name; ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label for="start_date">Enable On (UTC)</label>
                    <input type="text" name="start_date" value="<?= $category_wise_discounts->enable_on_utc; ?>" class="form_datetime form-control start_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="end_date">Disable On (UTC)</label>
                    <input type="text" name="end_date" value="<?= $category_wise_discounts->disable_on_utc; ?>" class="form_datetime form-control end_date" readonly="true">
                </div>
                <div class="form-group">
                    <label for="reduction_amount">Reduction Amount</label>
                    <div class="row">
                        <div class="col-md-6">
                            <select name="reduction_type" class="form-control">
                                <option value="<?= $category_wise_discounts->reduction_type; ?>" selected><?= $category_wise_discounts->reduction_type; ?></option>
                                <option value="percent">Percentage</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="reduction_amount" value="<?= $category_wise_discounts->percentage; ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <div class="form-group">
                        <select name="enabled_1" class="form-control">
                            <option value="1"  {{$category_wise_discounts->status == 1 ? 'selected' : ''}}>Enabled</option>
                            <option value="0"  {{$category_wise_discounts->status == 0 ? 'selected' : ''}}>Disabled</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-1 well pull-right">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Select MainCategory</label>
                            <select class="form-control maincategory" name="maincategory_id" id="maincategory">
                                <option value="">SELECT</option>
                                @foreach ($maincategory as $maincategortitem)
                                <option value="{{$maincategortitem->id}}"  {{ $category_wise_discounts->maincategory_id == $maincategortitem->id ? 'selected' : ''}} >{{$maincategortitem->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Select Category</label>
                            <select class="form-control primary_category" name="primary_category" id="primary_category" required>
<!--                                <option value="">SELECT</option>-->
                                <?php
                                foreach($categories as $category)
                                {
                                    ?>
                                    <option value="<?= $category->id; ?>" <?php if($category->id == $category_wise_discounts->category_id) { echo "selected"; } ?>><?= $category->cat_name; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Select Sub Category</label>
                            <select class="form-control secondary_category" name="secondary_category" id="secondary_category" required>
<!--                                <option value="">SELECT</option>-->
                                <?php
                                $subcategories = App\subcategories::where('categories_id',$category_wise_discounts->category_id)->get();
                                foreach($subcategories as $subcategory)
                                {
                                    ?>
                                    <option value="<?= $subcategory->id; ?>" <?php if($subcategory->id == $category_wise_discounts->sub_category_id) { echo "selected"; } ?>><?php echo $subcategory->sub_cat_name; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            <div class="col-md-2"></div>
        </div>
    </form>



    <hr>
    <footer></footer>
</div>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function(){
        $("#primary_category").change(function(){
            var $catId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getsubcategories') }}",
                data: {categoryid:$catId},
                success: function(result)
                {
                    $('#secondary_category').html(result);
                }
            });

        });

        $("#maincategory").change(function(){
            var $maincatId = this.value;

            $.ajax({
                type: "POST",
                url: "{{ url('admin/getcategories') }}",
                data: {maincategoryid:$maincatId},
                success: function(result)
                {
                    $('#primary_category').html(result);
                }
            });
        });
    });
</script>
@endsection