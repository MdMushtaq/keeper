@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Products Info</h1>
</div>
<div class="row">
    <br>
    <div class="col-md-6">
        {{-- <h3>Best Sellers</h3> --}}
    </div>
    <div class="col-md-6">
        <form class="form-inline pull-right">
            <input class="form-control datepicker" type="text" name="best_sellers_start" placeholder="From"/>
            <input class="form-control datepicker" type="text" name="best_sellers_end" placeholder="To"/>

            <input class="btn btn-primary" type="button" value="Get Best Sellers"/>
        </form>
    </div>
</div>
<br>
<div class="btn-group pull-right">
</div>
    <table class="table table-striped" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            {{-- <th>S.No</th> --}}
            <th>Product Name</th>
            <th>Total Quantity</th>
            <th>Sale</th>
            <th>Balance</th>
        </tr>
        </thead>
        <tbody>
            {{-- @php $serial = 1; @endphp --}}
            @foreach ($best_sell as $report)
                @foreach ($report->order_item->groupBy('product_id') as $items)
                    @foreach ($report->productQuantity->groupBy('productid') as $quantityitem)
                    <tr>
                        {{-- <td>{{$serial++}}</td> --}}
                        <td>{{ $items->first()->product_name  }}</td>

                        <td>{{$quantityitem->sum('quantity')}}</td>

                        <td>{{ $items->sum('product_quantity') }}</td>
                    
                        <td>{{ $quantityitem->sum('quantity') - $items->sum('product_quantity') }}</td>
                    </tr>
                    @endforeach
                @endforeach
            @endforeach
        </tbody>
    </table>
 

@endsection