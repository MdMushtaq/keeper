@extends('layouts.admin-app')
@section('content')
<div class="page-header">
	<h1>Sales Info</h1>
</div>
<div class="row">
    <br>
    <div class="col-md-6">
        {{-- <h3>Best Sellers</h3> --}}
    </div>
    <div class="col-md-6">
        <form class="form-inline pull-right">
            <input class="form-control datepicker" type="text" name="best_sellers_start" placeholder="From"/>
            <input class="form-control datepicker" type="text" name="best_sellers_end" placeholder="To"/>

            <input class="btn btn-primary" type="button" value="Get Best Sellers"/>
        </form>
    </div>
</div>
<br>

    <table class="table table-striped" cellspacing="0" cellpadding="0">
        <thead>
        <tr>
            <th>Date</th>
            <th>Invoice Number</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Item</th>
            <th>Unit Price</th>
            <th>Quantity</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php $total = 0; ?>
        @foreach($orders as $order)
            @foreach ($order->orderitems as $item)
            <tr>
                <td>{{ $order->created_at }}</td>
                <td></td>
                <td>
                    @php
                    $user = App\User::where("id",$order->user_id)->first();
                    echo $user->name;
                    @endphp
                </td>
                <td>
                    @php
                    $user = App\User::where("id",$order->user_id)->first();
                    echo $user->lastname;
                    @endphp
                </td>
                <td>
                    {{ $item->product_name }}
                </td>
                <td>{{ $item->product_unit_price }}</td>
                <td>{{ $item->product_quantity }}</td>
                <td>{{ $item->product_unit_price*$item->product_quantity }}</td>
                <?php
                $totalAmount = $item->product_unit_price*$item->product_quantity;
                $total = $total+$totalAmount;
                ?>
            </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-md-8"></div>
        <div class="col-md-4">
            <table class="table" border="1">
                <tr>
                    <td>Total Amount</td>
                    <td><span>{{ $total }}</span></td>
                </tr>
            </table>
        </div>
    </div>
 

@endsection