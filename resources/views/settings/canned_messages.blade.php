@extends('layouts.admin-app')
@section('content')

<div class="page-header">
	<h1>Canned Messages</h1>
</div>
<div class="text-right">
    <a class="btn btn-primary" href="add-canned-message.php">
    	<i class="icon-plus"></i> Add Canned Message
    </a>
</div>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Message Name</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Gift Card Email Notification</td>
            <td class="text-right">
                <span class="btn-group">
                    <a class="btn btn-default" href="#">
                    	<i class="icon-pencil"></i>
                    </a>
               	</span>
            </td>
        </tr>
        <tr>
            <td>Reset Customer Password</td>
            <td class="text-right">
                <span class="btn-group">
                    <a class="btn btn-default" href="#">
                    	<i class="icon-pencil"></i>
                    </a>
               	</span>
            </td>
        </tr>   
    </tbody>
</table>

@endsection