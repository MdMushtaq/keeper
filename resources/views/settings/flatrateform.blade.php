@extends('layouts.admin-app')
@section('content')
<div class="page-header">
    <h1>Flate Rate Configuration</h1>
</div>
<script>
    var fieldarray = [];
</script>
@if(\Session::has('danger'))
<div class="alert alert-danger">
    <p>{{ \Session::get('danger') }}</p>
</div>
@endif
<form action="{{ url('admin/flat-rate/form') }}" enctype="multipart/form-data" method="post" accept-charset="utf-8">
    {{ csrf_field() }}
    <table class="table">
        <thead>
        <tr>
            <th style="width:20%;">Select Flate Rate</th>
            <th style="width:20%;">Flate Rate</th>
            <th style="text-align:right;">
                <div class="col-md-4 input-group pull-right">
                    <input type="number" value="" class="form-control" id="new_order_status_field" style="margin:0px;" placeholder="Flate Rate">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-success" onclick="add_status()"><i class="icon-plus"></i></button>
                    </div>
                </div>
            </th>
        </tr>
        </thead>
        <tbody id="orderStatuses">
        <?php
        foreach($flatrates as $flatrate)
        {
            ?>
            <tr>
                <td>
                    <input type="radio" name="selectrate" value="<?= $flatrate->id; ?>" required <?php if($flatrate->flatrateselect == 1) { echo "checked"; } ?>>
                    <input type="hidden" name=flatrateid[]" value="<?= $flatrate->id; ?>">
                    <input type="hidden" value="<?= $flatrate->flatrate; ?>" name="rate[]" >
                </td>
                <td><?= $flatrate->flatrate; ?></td>
                <td style="text-align:right;">
                    <!--                        <button type="button" class="removeOrderStatus btn btn-danger" value="5">-->
                    <!--                            <i class="icon-close"></i>-->
                    <!--                        </button>-->
                    <a class="btn btn-danger" href="{{ route('flatrate.delete',$flatrate->id) }}" onclick="return confirm('are you sure?')"><i class="icon-times"></i></a>

                </td>
            </tr>
            <script>
                var a = "<?php echo $flatrate->flatrateselect; ?>";
                fieldarray.push(a);
            </script>
        <?php
        }
        ?>
        </tbody>

    </table>

    <!--    <textarea name="vat_rates" cols="40" rows="10" id="order_statuses_json"></textarea>-->
    <!--    <input type="radio" id="male" name="gender" value="male">-->
    <!--    <label for="male">Male</label><br>-->

    <input type="submit" class="btn btn-primary" value="Save">

</form>
<span style="display: none;" id="maxvatid" value=""></span>
<!--<script type="text/template" id="orderStatusTemplate">-->
<!--    <tr>-->
<!--        <td>-->
<!--            <input type="radio" value="" name="rate">-->
<!--        </td>-->
<!--        <td>-->
<!---->
<!--        </td>-->
<!--        <td style="text-align:right;">-->
<!--            <button type="button" class="removeOrderStatus btn btn-danger" value=""><i class="icon-close"></i></button>-->
<!--        </td>-->
<!--    </tr>-->
<!--</script>-->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
<script>

    var orderStatus = "5";
    var orderStatuses = {"5":"5"};
    var orderStatusTemplate = $('#orderStatusTemplate').html();

    function renderOrderStatus()
    {
        $('#orderStatuses').html('');
        $.each(orderStatuses, function(id, val){
            var data = {status:val}
            var output = Mustache.render(orderStatusTemplate, data);
            $('#orderStatuses').append(output);
            $('input[value="'+orderStatus+'"]').prop('checked', true);
        });
        //update the order_statuses_json field
        $('#order_statuses_json').val( JSON.stringify(orderStatuses) );
    }

    var maxflatrateidd = '';
    function add_status()
    {

        var status = $('#new_order_status_field').val();

        if(status == '')
        {
            alert("Add Shipping");
        }
        else
        {
            var checkingIndex = fieldarray.indexOf(status);
            if(checkingIndex>=0)
            {
                alert("Value added before");
            }
            else
            {
                var maxvatid;

                $(document).ready(function(){
                    $.ajax({
                        type: "POST",
                        url: "{{ url('admin/getmaxshippingid') }}",
                        success: function(result)
                        {
                            maxvatid = ++result;
                            $('#maxvatid').html(maxvatid);
                        }
                    });
                });


                if(maxflatrateidd == "")
                {
                    maxflatrateidd = "<?php echo $maxFlatrateId; ?>";
                    maxflatrateidd = ++maxflatrateidd;
                }
                else
                {
                    maxflatrateidd = ++maxflatrateidd;
                }
                var tablerow = '<tr><td><input type="radio" name="selectrate" value="'+maxflatrateidd+'" required> <input type="hidden" value="'+status+'" name="rate[]"></td><td>'+status+'</td><td style="text-align:right;"><button type="button" class="removeOrderStatus btn btn-danger" value="5"><i class="icon-close"></i></button></td></tr>';

                $("#orderStatuses").last().append(tablerow);
            }
        }
    }

    function deleteStatus(status)
    {
        delete orderStatuses[status];
        renderOrderStatus();
    }


</script>
@endsection